﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using EpamTicketoTask.Data.Abstractions;
using EpamTicketoTask.Data.SQLite;
using EpamTicketoTask.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Globalization;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Localization;
using System;
using System.Threading;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using EpamTicketoTask.BusinessLogic.Abstractions;
using EpamTicketoTask.BusinessLogic.Services;

namespace EpamTicketoTask.EmptyTemplate
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940

        public IConfigurationRoot Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<StorageContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped(typeof(IStorage), typeof(Storage));
            services.AddIdentity<User, UserRole>()
                .AddEntityFrameworkStores<StorageContext>()
                .AddUserManager<CustomUserManager>();
            //.AddUserManager<CustomUserManager>()
            //.AddUserStore<CustomUserStore>()
            //.AddRoleStore<CustomRoleStore>()
            //.AddDefaultTokenProviders();

            services.AddScoped(typeof(IServiceStorage),typeof(ServiceStorage));

            services.Configure<IdentityOptions>(options =>
            {
                options.Cookies.ApplicationCookie.LoginPath = new Microsoft.AspNetCore.Http.PathString("/Auth/Login");
            });

            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[]
                {
                    new CultureInfo("en"),
                    new CultureInfo("be"),
                    new CultureInfo("ru")
                };
                options.DefaultRequestCulture = new RequestCulture("be");

                options.SupportedCultures = supportedCultures;

                options.SupportedUICultures = supportedCultures;

            });
            services.AddMvc()
            .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
            .AddDataAnnotationsLocalization();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();
            app.UseIdentity();

            app.UseCookieAuthentication();

            var locOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(locOptions.Value);

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            DataInitializer.CreateRolesandUsersAsync(app.ApplicationServices).Wait();
            DataInitializer.CreateData(app.ApplicationServices);
        }

        
    }
}
