﻿using EpamTicketoTask.Data.Abstractions;
using EpamTicketoTask.Data.Abstractions.EntityRepositories;
using EpamTicketoTask.Data.SQLite;
using EpamTicketoTask.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Collections;

namespace EpamTicketoTask.EmptyTemplate
{
    public class DataInitializer
    {

        public static async Task CreateRolesandUsersAsync(IServiceProvider serviceProvider)
        {
            UserManager<User> userManager = serviceProvider.GetRequiredService<UserManager<User>>();
            RoleManager<UserRole> roleManager = serviceProvider.GetRequiredService<RoleManager<UserRole>>();

            if (await roleManager.FindByNameAsync("admin") == null)
            {
                await roleManager.CreateAsync(new UserRole("1", "admin"));
            }
            if (await roleManager.FindByNameAsync("user") == null)
            {
                await roleManager.CreateAsync(new UserRole("2", "user"));
            }

            if (await userManager.FindByNameAsync("Admin") == null)
            {
                User user = new User("Admin", "Admin", "George", "Belomutov", new CultureInfo("en"), "Gomel Volgogradskaya 32", "+375-29-839-83-17");
                IdentityResult result = await userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(user, "admin");
                }
            }
            if (await userManager.FindByNameAsync("user") == null)
            {
                User user = new User("user", "user", "UserName", "Famillievich", new CultureInfo("ru"), "Gomel Volgogradskaya 32", "+375-29-839-83-17");
                IdentityResult result = await userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(user, "user");
                }
            }
            if (await userManager.FindByNameAsync("georg") == null)
            {
                User user = new User("georg", "123", "Petr", "Ivanov", new CultureInfo("be"), "Minsk Pushkina-Kolotushkina", "+375-11-22-33-44");
                IdentityResult result = await userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(user, "user");
                }

            }
            if (await userManager.FindByNameAsync("kagor") == null)
            {
                User user = new User("kagor", "qwerty", "Ivan", "Petrov", new CultureInfo("en"), "Gomel Volgogradskaya 37", "+375-29-839-83-17");
                IdentityResult result = await userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(user, "user");
                    await userManager.AddToRoleAsync(user, "admin");
                }

            }

        }

        public static void CreateData(IServiceProvider serviceProvider)
        {
            UserManager<User> um = serviceProvider.GetRequiredService<UserManager<User>>();

            IStorage dataStorage = serviceProvider.GetService<IStorage>();

            var Cities = dataStorage.GetRepository<ICityRepository>();
            if (!Cities.All().Any())
            {
                Cities.Create(new City("1", "Brest"));
                Cities.Create(new City("2", "Vitebsk"));
                Cities.Create(new City("3", "Gomel"));
                Cities.Create(new City("4", "Grodno"));
                Cities.Create(new City("5", "Minsk"));
                Cities.Create(new City("6", "Mogilev"));

                dataStorage.Save();
            }

            var Venues = dataStorage.GetRepository<IVenueRepository>();
            if (!Venues.All().Any())
            {

                Venues.Create(new Venue("1", "Minsk-Arena", "просп. Победителей 111", Cities.Get("5")));
                Venues.Create(new Venue("2", "Mogilev Airport", "п/а Княжыцы, Аэрапорт", Cities.Get("6")));
                Venues.Create(new Venue("3", "Stadium Tractor", "улица Ванеева 3, Минск", Cities.Get("5")));
                Venues.Create(new Venue("4", "Cuba", "ул. Первомайская 29", Cities.Get("6")));
                Venues.Create(new Venue("5", "Дворец водных видов спорта", "ул. Московская 147", Cities.Get("1")));
                Venues.Create(new Venue("6", "Кинотеатр Мир", "ул. Пушкинская 7", Cities.Get("1")));
                Venues.Create(new Venue("7", "Дынама", "просп. Генерала Людникова 12", Cities.Get("2")));
                Venues.Create(new Venue("8", "Спорткомплекс \"Локомотив\"", "улица Леонова", Cities.Get("2")));
                Venues.Create(new Venue("9", "Стары замак", "Замковый туп. улица Замковая, 22", Cities.Get("4")));
                Venues.Create(new Venue("10", "Дом масонаў", "ул. Телеграфная 12", Cities.Get("4")));
                Venues.Create(new Venue("11", "Молодёжный театр", "просп. Ленина 10", Cities.Get("3")));
                Venues.Create(new Venue("12", "Драматычны тэатр", "пл. Ленина 1", Cities.Get("3")));

                dataStorage.Save();
            }

            var Events = dataStorage.GetRepository<IEventRepository>();
            if (!Events.All().Any())
            {
                Events.Create(new Event("1", "Depeche Mode concert", new DateTime(2017, 7, 17, 20, 0, 0), Venues.Get("1"), "DepecheMode.jpg",
                "На вчерашней пресс-конференции <b>Depeche Mode</b> объявили, что 17 июля 2017 года выступят в Минске, а сегодня белорусы уже могут приобрести билеты на концерт. Их стоимость варьирует" +
                "ся от 80 до 230 рублей. " +
                "Depeche Mode молчали долгих три года, но, как оказалось, по уважительной причине: они готовили новый альбом Spirit и гастрольную программу Global Spirit Tour, которую увидят более пол" +
                "утора миллиона поклонников из 21 европейской страны.И в списке есть белорусы!Похоже, «Депешей» впечатлила потрясающая атмосфера на «Минск - Арене»: во время прошлых концертов белорусс" +
                "кая публика зажигала просто неистово. " +
                "Помимо нашей страны, группа выступит в родной Великобритании, заедет в Германию, Францию, Италию, Испанию… Тур стартует 5 мая в шведском Стокгольме и завершится 23 июля в румынском го" +
                "роде Клуж - Напока." +
                "Чем удивят музыканты на этот раз ? Однозначно – ярким визуальным рядом: Depeche Mode славятся потрясающими шоу, каждое из которых совершенно не похоже на предыдущие.Но главное – новым" +
                " альбомом, над которыми «Депеши» трудились так долго.Все предыдущие 13 работ коллектива были более чем успешны: например, прошлый диск Delta Machine попал в топ - 5 музыкальных чартов в 21 стране и в 12 странах занял первое место. " +
                "Очевидно, что Depeche Mode горят желанием подтвердить свой статус бесперебойного поставщика хитов на музыкальный рынок.Недаром участники группы подтянули «тяжелую артиллерию»: как ста" +
                "ло известно на пресс - конференции, продюсирует диск Джеймс Форд.В его послужном списке бестселлеры Arctic Monkeys и Florence And The Machine.Сам Дейв Гаан в успехе Spirit даже не сомневается: " +
                "– Мы гордимся этим альбомом, его звучание впечатляет.Он свежий и записан отличными музыкантами при участии отличных продюсеров.И в нем полно замечательных песен. " +
                "И конечно, не обойдется без главных боевиков группы.Публика наверняка услышит Personal Jesus, которую влиятельный музыкальный журнал Q включил в список «100 величайших песен всех вре" +
                "мен». Выход композиции сопровождала креативная рекламная компания как раз в духе «Депешей»: в газетах были размещены объявления с интригующим текстом «Ваш личный Иисус» и номером теле" +
                "фона.При звонке на этот номер автоответчик проигрывал будущий мировой хит, который белорусы совсем скоро услышат в живом исполнении. " +
                "Еще одна культовая композиция Enjoy the Silence, оказывается, могла звучать совсем иначе: Мартин Гор задумывал ее как мелодичную лирическую балладу в минорной тональности.Но тогдашний" +
                " клавишник группы Алан Уайлдер так отчетливо видел ее в качестве заводного танцевального трека, что спорил с самим автором, пока не переубедил его.Впоследствии песня прозвучала еще во м" +
                "ножестве разных версий: ее перепевали HIM, Тори Эймос, Keane, Lacuna Coil, Gregorian и многие другие, а Linkin Park создали ремикс вместе с самими Depeche Mode.Ну а «Минск - Арену» снов" +
                "а раскачает оригинальная Enjoy the Silence с магическим вокалом Дейва Гаана. " +
                "Раз уж речь зашла о «Минск - Арене»: это одна из самых маленьких площадок Global Spirit Tour.По меркам привычных к стадионам «Депешей» – почти квартирник.Так что белорусы увидят своих" +
                " любимцев ближе всех, в какой бы точке зала они не находились. Depeche Mode выступят в Минске в рамках Global Spirit Tour 17 июля 2017 года на «Минск - Арене». Начало в 20.00."));


                Events.Create(new Event("2", "Дрифт Могилев - 2 этап Чемпионата Беларуси по дрифтинг", new DateTime(2017, 6, 4), Venues.Get("2"), "Drift.png", "Второй этап Чемпионата Беларуси по дрифтингу запомнился зрителям острыми " +
                    "моментами от которых захватывает дыхание, красивыми и дымными заездами, синхронными перекладками и четверкой лучших из лучших: Сергей Сак на BMW E46 (команда G-Drive, Беларусь), Павел " +
                    "Бусыгин на Toyota Supra (команда ToyoTires x GorillaEnergy, Россия), Сергей Кремец на Mersedes-Benz 190 (команда No Motors, Украина), Макс Миллер на Lexus IS (команда No Motors, Украин" +
                    "а).Впервые за восемь лет Чемпионата Беларуси по дрифтингу подиум заняли представители трёх стран.Россиянин Павел Бусыгин в финальной гонке обошел чемпиона 2016 года Сергея Сака и подня" +
                    "лся на первую ступеньку.Бронза досталась представителю украинской команды No Motors Сергею Кремецу.«Новая конфигурация трассы понравилась абсолютно всем спортсменам.Даже тем, которые е" +
                    "е не одолели.Они поняли, что дрифт может быть другим: еще более зрелищным и экстремальным.Очень символично, что в тройку лидеров вошли пилоты из Беларуси, России и Украины.Этот этап мож" +
                    "но смело назвать славянским кубком.Мы верим, что спорт объединяет народы.И сегодня ребята это доказали, — прокомментировал руководитель гонки Айк Симонян. Следующие соревнования пройду" +
                    "т на территории аэропорта «Минск - 1». Восточноевропейский Кубок по дрифтингу(EEDC) намечен на 25 июня.Каждый год на гонку в центре белорусской столицы приезжают лучшие пилоты Востока " +
                    "и Запада, а также звезды мирового дрифта.EEDC – крупнейшее дрифт - событие, которое собирает максимальное количество участников и зрителей из разных стран и привлекает внимание прессы."));



                Events.Create(new Event("3", "Футбол: Беларусь U-21 - Сан-Марино U-21", new DateTime(2017, 6, 7), Venues.Get("3"), "FootBall.jpg", "В квалификации нынешнего континентального первенства участвуют сборные 54 стран, расп" +
                    "ределенные жребием на 9 групп. Отборочные встречи пройдут в два круга до октября 2018 года. В финальную часть, в которой в июне 2019-го выступят 12 сборных, выйдут 9 победителей групп," +
                    " а также выявленные в стыковых матчах с 12 по 20 ноября 2018-го 2 сильнейшие из 4 лучших вторых команд в группах. К ним добавится хозяин финального турнира Италия. Сан - Марино вместе с" +
                    " Италией является страной - хозяйкой молодежного EURO - 2019, но если итальянцы освобождены от отборочного турнира, то <b>карликам</b> из Сан - Марино такой привилегии предоставлять не стали." +
                    "В рамках подготовки к старту квалификации предстоящие соперники белорусов в мае провели спарринги с любительским клубом </i>Фиа Риччоне</i> — 2:1(Томассини — 2) и национальной сборной Сан - Ма" +
                    "рино — 0:1.Рулевой <i>молодежки</i> — Фабрицио Костантини.Он был назначен на этот пост в феврале нынешнего года, сменив Мирко Папини."));



                Events.Create(new Event("4", "Wonder Woman Movie", new DateTime(2017, 1, 6), Venues.Get("4"), "WonderWoman.png", "Перед тем как стать Чудо-Женщиной, она была Дианой — принцессой амазонок, обученной быть непобедимой " +
                    "воительницей. И когда на берегах ограждённого от внешнего мира райского острова, который служил ей родиной, терпит крушение американский пилот и рассказывает о серьёзном конфликте, " +
                    "бушующем во внешнем мире, Диана покидает свой дом, чтобы справиться с этой угрозой. И там, сражаясь бок о бок с человеком в войне за мир, Диана обнаружит всю полноту своей власти… и" +
                    " своё истинное предназначение."));

                Events.Create(new Event("5", "Cirque du Soleil - Varekai", new DateTime(2017, 9, 30, 20, 00, 00), Venues.Get("5"), "Cirque.jpg",
                    "<p></b>О Varekai</b></p>" +
                    " В глубине загадочного леса, на вершине вулкана находится волшебный мир, в котором нет ничего невозможного.Этот мир называется Varekai.С небес на землю падает одинокий юноша, " +
                    "и история Varekai начинается.Очутившись среди теней таинственного леса, в пестром мире фантастических созданий главный герой окунается в бурный поток неожиданных и захватывающ" +
                    "их приключений.На грани времен, в мире, где возможно все и даже больше, звучит вдохновенный гимн переосмыслению жизни и вновь обретенному чуду, таящимся в основе мироздания и " +
                    "нашей души. — Cirque du Soleil счастлив вернуться в Беларусь и впервые представить здесь одно из своих классических шоу – Varekai.На протяжении последних трех лет это шоу соби" +
                    "рало аншлаги на аренах в Европе, России и Северной Америке.Varekai – это история о любви и надежде.О способности найти в себе смелость идти вперед и открываться новому опыту д" +
                    "аже в трудное время.Это история о том, что каждый из нас переживает хотя бы раз в жизни.Varekai – это смесь ошеломительной акробатики, хореографии, живой музыки и веселой кло" +
                    "унады, — отмечает Бруно Дарманьяк, арт - директор Varekai. Каждый режиссер, которому выпадает честь ставить шоу для Cirque du Soleil, оставляет в цирке частичку своей души.Не" +
                    " стал исключением и режиссер Varekai — Доминик Шампань, известный канадский драматург, сценарист и театральный постановщик, обладатель многочисленных театральных премий, сред" +
                    "и которых премия Ассоциации критиков Critics’ Prize и гран - при театрального фестиваля в Монреале.Работу этого режиссера белорусы увидят впервые. Костюмы для шоу разрабатывал" +
                    "а лауреат премии Оскар Эйко Исиока. Композитор Varekai Виолейн Корради объединила песни французских трубадуров XI века, традиционные госпелы, армянские мелодии и ритмы гавайс" +
                    "ких ритуальных церемоний.Музыка в шоу Cirque du Soleil исполняется вживую, а исполнители постоянно находятся на сцене во время спектакля. Varekai посмотрели более 10 миллионо" +
                    "в человек в 130 городах мира, что позволяет с полным правом назвать его блокбастером, наряду с кассовыми спектаклями и фильмами." +
                    "<p></b>О Cirque du Soleil</b></p>" +
                    " В 2013 году, когда Cirque du Soleil дебютировал в Беларуси, его спектакли перевернули представление жителей нашей страны о цирковом искусстве.Яркие, фантасмагоричные образы и" +
                    " персонажи шоу, сама обстановка, сопровождавшая выступления, – тогда казалось удивительным, что в цирковой постановке могут быть не только трюки, но внятная история, и живой " +
                    "вокал. Начав с небольшой группы из 20 уличных артистов в 1984 году, Cirque du Soleil превратился в одну из крупнейших квебекских компаний, которая создает художественные пост" +
                    "ановки высочайшего качества.В компании трудится почти 4000 человек из 50 стран мира, в том числе 1300 артистов. Западные театральные критики пишут о Cirque du Soleil как о т" +
                    "оржестве чувств с яркими костюмами, движущейся музыкой, трансформирующими пространство звуковыми эффектами и удивительными подвигами человеческого тела и духа. Cirque du Sole" +
                    "il принес сказку и радость в жизнь более чем 160 миллионов зрителей в 400 с лишним городах в 60 странах на пяти континентах.Более детальную информацию о Cirque du Soleil можн" +
                    "о найти на сайте <a href=\"www.cirquedusoleil.com\">www.cirquedusoleil.com.</a>"));

                Events.Create(new Event("6", "Lebibgrad Concert", new DateTime(2017, 12, 1, 20, 00, 00), Venues.Get("4"), "Leningrad.jpg",
                    "Невероятный подарок делает группировка «Ленинград» всем поклонникам из Беларуси — объявлен дополнительный концерт для тех многочисленных людей, которые по объективным причинам " +
                    "не смогли купить билеты на концерт 19 марта. — Все белорусы, любящие «Ленинград», должны быть удовлетворены.Цены на концерт останутся такими же, как и на концерт 19 марта, — пр" +
                    "окомментировали запуск второго концерта его организаторы, агентство «Атом Интертеймент». Несмотря на то, что на официальном сайте «Ленинграда», Минска - 2 пока что нет в график" +
                    "е, лидер группировки «Ленинград» Сергей Шнуров лично подтвердил, что 1 декабря будет снова играть в «Минск - арене». — Да, это правда, — подтвердил в телефонном разговоре Серге" +
                    "й Шнуров, — Когда нам сказали, что продана вся «Минск - арена», но купить билеты успели не все белорусы, мы начали искать дату, когда можно провести дополнительный концерт.Пазл" +
                    " сошелся на 1 декабря.Не пропустите вспышку! Кстати, минчанам есть, чем гордиться: за всю историю группы принять два выступления «Ленинграда» за год посчастливилось только трем" +
                    " городам – Москве, Питеру и вот теперь Минску.Только чтобы наверняка успеть на выступление группировки «Ленинград», о билетах стоит позаботиться заранее – третьего концерта в 2" +
                    "017 - м не будет точно. За девять месяцев, которые пройдут с мартовского по декабрьский концерт в Минске, «Ленинград» успеет отыграть юбилейную программу во всех крупнейших гор" +
                    "одах, зарулит в Европу и слетает в Штаты.В гастрольном графике группы Череповец, Пермь, Волгоград, Казань, Нижний Новгород, Варшава, Париж, Лондон, Кишинев, Новосибирск, Хабаро" +
                    "вск, Владивосток, Нью - Йорк, Тель - Авив, Берлин, Будапешт, Дюссельдорф, Сан - Франциско, Ростов - на - Дону, Краснодар. В этом году «передвижному цирку», как называет свою ком" +
                    "анду Сергей Шнуров, исполняется 20 лет.Это означает, что первое поколение поклонников заводной группировки за это время успело закончить не только школу и университет, но и обз" +
                    "авестись детьми, многие из которых тоже уже выросли.Так что на выступления группировки «Ленинград» ходят целыми семьями, всерьез обсуждая в тематических группах в социальных се" +
                    "тях, точно ли маму пустят на концерт или существует верхняя планка по возрасту. Организатор: ООО <b>«Атом М»</b>"

                    ));

                dataStorage.Save();
            }
            //User users =((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "olo");

            var Tickets = dataStorage.GetRepository<ITicketRepository>();
            if (!Tickets.All().Any())
            {
                Tickets.Create(new Ticket("1", Events.Get("1"), 222,
                    ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "user")));
                Tickets.Create(new Ticket("2", Events.Get("1"), 225,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "admin")));
                Tickets.Create(new Ticket("3", Events.Get("1"), 221,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "georg")));
                Tickets.Create(new Ticket("4", Events.Get("1"), 422,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "kagor")));
                Tickets.Create(new Ticket("5", Events.Get("2"), 425,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "user")));
                Tickets.Create(new Ticket("6", Events.Get("2"), 421,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "admin")));
                Tickets.Create(new Ticket("7", Events.Get("2"), 223,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "georg")));
                Tickets.Create(new Ticket("8", Events.Get("2"), 251,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "kagor")));
                Tickets.Create(new Ticket("9", Events.Get("3"), 215,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "user")));
                Tickets.Create(new Ticket("10", Events.Get("3"), 222,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "admin")));
                Tickets.Create(new Ticket("11", Events.Get("3"), 225,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "georg")));
                Tickets.Create(new Ticket("12", Events.Get("3"), 221,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "kagor")));
                Tickets.Create(new Ticket("13", Events.Get("4"), 422,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "user")));
                Tickets.Create(new Ticket("14", Events.Get("4"), 425,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "admin")));
                Tickets.Create(new Ticket("15", Events.Get("4"), 421,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "georg")));
                Tickets.Create(new Ticket("16", Events.Get("4"), 223,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "kagor")));
                Tickets.Create(new Ticket("17", Events.Get("5"), 251,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "user")));
                Tickets.Create(new Ticket("18", Events.Get("5"), 215,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "admin")));
                
                dataStorage.Save();
            }

            var Orders = dataStorage.GetRepository<IOrderRepository>();
            if (!Orders.All().Any())
            {
                Orders.Create(new Order("01", Tickets.Get("4"),
                    OrderStatuses.WaitingConfirmation,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "user")));
                Orders.Create(new Order("02", Tickets.Get("4"),
                    OrderStatuses.WaitingConfirmation,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "admin")));
                Orders.Create(new Order("03", Tickets.Get("4"),
                    OrderStatuses.WaitingConfirmation,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "georg")));
                Orders.Create(new Order("04", Tickets.Get("8"),
                   OrderStatuses.WaitingConfirmation,
                  ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "user")));
                Orders.Create(new Order("05", Tickets.Get("8"),
                    OrderStatuses.WaitingConfirmation,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "admin")));
                Orders.Create(new Order("06", Tickets.Get("8"),
                    OrderStatuses.WaitingConfirmation,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "georg")));
                Orders.Create(new Order("07", Tickets.Get("16"),
                   OrderStatuses.WaitingConfirmation,
                  ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "user")));
                Orders.Create(new Order("08", Tickets.Get("16"),
                    OrderStatuses.WaitingConfirmation,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "admin")));
                Orders.Create(new Order("09", Tickets.Get("16"),
                    OrderStatuses.WaitingConfirmation,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "georg")));
                Orders.Create(new Order("10", Tickets.Get("2"),
                   OrderStatuses.WaitingConfirmation,
                  ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "user")));
                Orders.Create(new Order("11", Tickets.Get("2"),
                    OrderStatuses.WaitingConfirmation,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "kagor")));
                Orders.Create(new Order("12", Tickets.Get("2"),
                    OrderStatuses.WaitingConfirmation,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "georg")));
                Orders.Create(new Order("42", Tickets.Get("10"),
                   OrderStatuses.WaitingConfirmation,
                  ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "user")));
                Orders.Create(new Order("13", Tickets.Get("10"),
                    OrderStatuses.WaitingConfirmation,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "kagor")));
                Orders.Create(new Order("14", Tickets.Get("10"),
                    OrderStatuses.WaitingConfirmation,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "georg")));
                Orders.Create(new Order("15", Tickets.Get("9"),
                  OrderStatuses.WaitingConfirmation,
                 ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "admin")));
                Orders.Create(new Order("16", Tickets.Get("9"),
                    OrderStatuses.WaitingConfirmation,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "kagor")));
                Orders.Create(new Order("17", Tickets.Get("9"),
                    OrderStatuses.WaitingConfirmation,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "georg")));
                Orders.Create(new Order("18", Tickets.Get("5"),
                 OrderStatuses.WaitingConfirmation,
                ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "admin")));
                Orders.Create(new Order("19", Tickets.Get("5"),
                    OrderStatuses.WaitingConfirmation,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "kagor")));
                Orders.Create(new Order("20", Tickets.Get("5"),
                    OrderStatuses.WaitingConfirmation,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "georg")));
                Orders.Create(new Order("21", Tickets.Get("1"),
                 OrderStatuses.WaitingConfirmation,
                ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "admin")));
                Orders.Create(new Order("22", Tickets.Get("1"),
                    OrderStatuses.WaitingConfirmation,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "kagor")));
                Orders.Create(new Order("23", Tickets.Get("1"),
                    OrderStatuses.WaitingConfirmation,
                   ((Storage)dataStorage).StorageContext.Users.FirstOrDefault(u => u.UserName == "georg")));


                dataStorage.Save();
            }

            /* foreach (City c in Cities.All())
              {
                  c.Venues = Venues.All().Where(x => x.City.Id == c.Id).ToList();
              }
              foreach (Venue v in Venues.All())
              {
                  v.Events = Events.All().Where(x => x.Venue.Id == v.Id).ToList();
              }
              foreach (Event e in Events.All())
              {
                  e.Tickets = Tickets.All().Where(x => x.Event.Id == e.Id).ToList();
              }
              foreach (Ticket t in Tickets.All())
              {
                  t.Orders = Orders.All().Where(x => x.Ticket.Id == t.Id).ToList();
              }*/

        }
    }
}
