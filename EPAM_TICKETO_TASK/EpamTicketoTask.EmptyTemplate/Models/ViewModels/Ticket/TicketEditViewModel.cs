﻿using System.ComponentModel.DataAnnotations;

namespace EpamTicketoTask.EmptyTemplate.Models.ViewModels.Ticket
{
    public class TicketEditViewModel
    {
        public string EventName { get; set; }
        public string Id { get; set; }
        public string EventId { get; set; }
        [Required]
        [Range(0.01,999999)]
        public decimal Price { get; set; }
        public string Notes { get; set; }
    }
}
