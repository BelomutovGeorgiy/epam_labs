﻿using EpamTicketoTask.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EpamTicketoTask.EmptyTemplate.Models.ViewModels.Ticket
{
    public class TicketIndexViewModel
    {
        public string Id { get; set; }
        public string EventId { get; set; }
        public string EventName { get; set; }
        public DateTime EventDate { get; set; }
        public decimal Price { get; set; }
        public string Notes { get; set; }
        public string SellerFullName { get; set; }
        public string SellerUserName { get; set; }
        public int OrderCount { get; set; }
        public bool IsSold { get; set; }
    }
}
