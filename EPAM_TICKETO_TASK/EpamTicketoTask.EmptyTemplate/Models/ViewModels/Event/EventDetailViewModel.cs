﻿using EpamTicketoTask.EmptyTemplate.Models.ViewModels.Ticket;
using Microsoft.AspNetCore.Html;
using System;
using System.Collections.Generic;

namespace EpamTicketoTask.EmptyTemplate.Models.ViewModels.Event
{
    public class EventDetailViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string VenueFull { get; set; }
        public string Photo { get; set; }
        public HtmlString Description { get; set; }

        public IEnumerable<TicketIndexViewModel> Tickets { get; set; }
    }
}
