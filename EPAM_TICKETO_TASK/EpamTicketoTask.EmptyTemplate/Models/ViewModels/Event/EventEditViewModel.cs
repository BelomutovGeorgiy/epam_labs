﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EpamTicketoTask.EmptyTemplate.Models.ViewModels.Event
{
    public class EventEditViewModel
    {
        public string Id { get; set; }
        [Required]
        [Display(Name = "EventName")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "EventData")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; } = DateTime.Now;
        [Required]
        [Display(Name = "VenueId")]
        public string VenueId { get; set; }
        [Display(Name = "Photo")]
        public IFormFile Photo { get; set; }
        [DataType(DataType.MultilineText)]
        [Display(Name = "Description")]
        public string Description { get; set; }
        public IEnumerable<SelectListItem> VenueList { get; set; }

        public string OldPhoto { get; set; }
    }
}
