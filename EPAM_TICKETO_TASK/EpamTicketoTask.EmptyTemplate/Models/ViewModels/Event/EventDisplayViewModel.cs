﻿using System;

namespace EpamTicketoTask.EmptyTemplate.Models.ViewModels.Event
{
    public class EventDisplayViewModel
    {

        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
        public string VenueFull { get; set; }
        public string Photo { get; set; }
    }
}
