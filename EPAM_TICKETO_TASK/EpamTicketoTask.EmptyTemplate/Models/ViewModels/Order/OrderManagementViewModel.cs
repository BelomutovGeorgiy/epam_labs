﻿using EpamTicketoTask.EmptyTemplate.Models.ViewModels.Ticket;
using System.Collections.Generic;

namespace EpamTicketoTask.EmptyTemplate.Models.ViewModels.Order
{
    public class OrderManagementViewModel
    {
        public TicketIndexViewModel Ticket { get; set; }

        public IEnumerable<OrderManagementListViewModel> Orders { get; set; }
    }
}
