﻿using System.ComponentModel.DataAnnotations;

namespace EpamTicketoTask.EmptyTemplate.Models.ViewModels.Order
{
    public class ConfirmViewModel
    {
        public string Id { get; set; }
        [Required]
        [Display(Name = "Reason")]
        public string TrackNo { get; set; }

        public string TicketId { get; set; }
    }
}
