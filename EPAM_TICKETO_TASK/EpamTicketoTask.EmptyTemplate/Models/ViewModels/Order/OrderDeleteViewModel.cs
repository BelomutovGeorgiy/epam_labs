﻿
namespace EpamTicketoTask.EmptyTemplate.Models.ViewModels.Order
{
    public class OrderDeleteViewModel
    {
        public string Id { get; set; }
        public string EventName { get; set; }
    }
}
