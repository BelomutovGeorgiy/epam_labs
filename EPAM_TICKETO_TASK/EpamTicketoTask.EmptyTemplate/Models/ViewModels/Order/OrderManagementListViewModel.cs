﻿using EpamTicketoTask.Data.Models;

namespace EpamTicketoTask.EmptyTemplate.Models.ViewModels.Order
{
    public class OrderManagementListViewModel
    {
        public string Id { get; set; }
        public string BuyerFullName { get; set; }
        public string BuyerPhoto { get; set; }
        public string Comment { get; set; }
        public OrderStatuses Status { get; set; }
        public string TrackNo { get; set; }
    }
}
