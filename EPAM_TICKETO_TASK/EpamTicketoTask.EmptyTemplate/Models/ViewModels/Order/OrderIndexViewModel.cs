﻿using EpamTicketoTask.Data.Models;

namespace EpamTicketoTask.EmptyTemplate.Models.ViewModels.Order
{
    public class OrderIndexViewModel
    {
        public string Id { get; set; }
        public string TicketId { get; set; }
        public string TicketEventId { get; set; }
        public string TicketEventName { get; set; }
        public decimal TicketPrice { get; set; }
        public OrderStatuses Status { get; set; }
        public string SellerFullName { get; set; }
        public string TicketNotes { get; set; }
        public string TrackNo { get; set; }
        public string Comment { get; set; }
    }
}
