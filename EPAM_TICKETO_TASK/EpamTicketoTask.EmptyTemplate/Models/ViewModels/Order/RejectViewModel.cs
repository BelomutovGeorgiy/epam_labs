﻿using System.ComponentModel.DataAnnotations;

namespace EpamTicketoTask.EmptyTemplate.Models.ViewModels.Order
{
    public class RejectViewModel
    {
        public string TicketId { get; set; }
        public string Id { get; set; }
        [Required]
        [Display(Name ="Reason")]
        public string Comment { get; set; }
    }
}
