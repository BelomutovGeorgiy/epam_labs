﻿using EpamTicketoTask.EmptyTemplate.Models.ViewModels.Auth;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace EpamTicketoTask.EmptyTemplate.Models.ViewModels.Auth
{
    public class AdminPanelViewModel
    {
        public IEnumerable<SelectListItem> RoleList { get; set; }
        public IEnumerable<UserRoleUpdateViewModel> UserList { get; set; }
    }
}
