﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace EpamTicketoTask.EmptyTemplate.Models.ViewModels.Auth
{
    public class UserEditViewModel
    {
        public string UserName { get; set; }
        [Display(Name="Avatar")]
        public IFormFile Photo { get; set; }
        [Display(Name = "FirstName")]
        [Required]
        public string FirstName { get; set; }
        [Display(Name = "LastName")]
        [Required]
        public string LastName { get; set; }
        [Display(Name = "Language")]
        [Required]
        public string Localization { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }
        [Display(Name = "Phone")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        public string PhotoPath { get; set; }
    }
}
