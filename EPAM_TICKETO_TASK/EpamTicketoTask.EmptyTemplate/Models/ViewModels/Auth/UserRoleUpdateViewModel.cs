﻿using System.Collections.Generic;

namespace EpamTicketoTask.EmptyTemplate.Models.ViewModels.Auth
{
    public class UserRoleUpdateViewModel
    {
        public string Photo { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public IEnumerable<string> RolesToAssign { get; set; }
        public IEnumerable<string> AllRoles { get; set; }
    }
}
