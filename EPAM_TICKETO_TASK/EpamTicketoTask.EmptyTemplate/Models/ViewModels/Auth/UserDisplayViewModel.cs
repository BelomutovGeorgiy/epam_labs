﻿
namespace EpamTicketoTask.EmptyTemplate.Models.ViewModels.Auth
{
    public class UserDisplayViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Photo { get; set; }
    }
}
