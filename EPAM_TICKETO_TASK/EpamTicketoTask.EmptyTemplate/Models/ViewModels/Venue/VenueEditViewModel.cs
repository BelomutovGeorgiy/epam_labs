﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EpamTicketoTask.EmptyTemplate.Models.ViewModels.Venue
{
    public class VenueEditViewModel
    {
        public string Id { get; set; }
        [Required]
        [Display(Name="Address")]
        public string Address { get; set; }
        [Required]
        [Display(Name = "City")]
        public string CityId { get; set; }
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        public IEnumerable<SelectListItem> CityList { get; set; }
    }
}
