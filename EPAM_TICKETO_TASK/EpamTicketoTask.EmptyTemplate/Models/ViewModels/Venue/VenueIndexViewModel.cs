﻿namespace EpamTicketoTask.EmptyTemplate.Models.ViewModels.Venue
{
    public class VenueIndexViewModel
    {
        public string Id { get; set; }
        public string Address { get; set; }
        public string CityName { get; set; }
        public string Name { get; set; }
    }
}
