﻿using System.ComponentModel.DataAnnotations;

namespace EpamTicketoTask.EmptyTemplate.Models.ViewModels.City
{
    public class CityViewModel
    {
        public string Id { get; set; }
        [Required]
        [Display(Name="Name")]
        public string Name { get; set; }
    }
}
