﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using EpamTicketoTask.BusinessLogic.Abstractions;
using EpamTicketoTask.BusinessLogic.Models.DTO;
using EpamTicketoTask.BusinessLogic.Infrastucture;
using AutoMapper;
using EpamTicketoTask.EmptyTemplate.Models.ViewModels.Ticket;
using EpamTicketoTask.EmptyTemplate.Models.ViewModels.Event;

namespace EpamTicketoTask.EmptyTemplate.Controllers
{
    public class TicketController : Controller
    {
        private IServiceStorage serviceStorage;

        public TicketController(IServiceStorage serviceStorage)
        {
            this.serviceStorage = serviceStorage;
        }

        [HttpGet]
        [Authorize]
        public IActionResult Add(string eventId, string eventName)
        {
            TicketEditViewModel model = new TicketEditViewModel
            {
                EventId = eventId,
                EventName = eventName
            };
            return View(model);
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public IActionResult Add(TicketEditViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            //Немного магии от автомаппера... она точно производительнее?
            Mapper.Initialize(cfg => cfg.CreateMap<TicketEditViewModel, TicketDTO>()
            .ForMember("SellerUserName",opt=>opt.MapFrom(src=>User.Identity.Name)));
            TicketDTO ticketDto = Mapper.Map<TicketEditViewModel, TicketDTO>(model);

            serviceStorage.GetService<ITicketService>().Make(ticketDto);
            return Redirect("~/Home/MyTickets");
        }

        [HttpGet]
        [Authorize]
        public IActionResult Edit(string id)
        {
            TicketDTO ticketDto = serviceStorage.GetService<ITicketService>().Get(id);

            if (ticketDto.SellerUserName == User.Identity.Name)
                return Redirect("~/Home/MyTickets");

            Mapper.Initialize(cfg => cfg.CreateMap<TicketDTO, TicketEditViewModel>());
            TicketEditViewModel model = Mapper.Map<TicketDTO,TicketEditViewModel>(ticketDto);

            return View(model);
        }
        [HttpPost]
        [Authorize]
        public IActionResult Edit(TicketEditViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            Mapper.Initialize(cfg => cfg.CreateMap<TicketEditViewModel, TicketDTO>()
            .ForMember("SellerUserName", opt => opt.MapFrom(src => User.Identity.Name)));
            TicketDTO ticketDto = Mapper.Map<TicketEditViewModel, TicketDTO>(model);

            serviceStorage.GetService<ITicketService>().Update(ticketDto);
            return Redirect("~/Home/MyTickets");
        }

        [HttpGet]
        [Authorize]
        public IActionResult Delete(string Id)
        {
            TicketDTO dtoModel = serviceStorage.GetService<ITicketService>().Get(Id);

            Mapper.Initialize(cfg => cfg.CreateMap<TicketDTO, TicketIndexViewModel>());
            TicketIndexViewModel model = Mapper.Map<TicketDTO, TicketIndexViewModel>(dtoModel);
            return View(model);
        }
        [HttpPost]
        [Authorize]
        [ActionName("Delete")]
        public IActionResult DeleteConfirm(EventDisplayViewModel model)
        {
            try
            {
                serviceStorage.GetService<ITicketService>().Delete(model.Id);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError("ConEv", e.Message);
                return View(model);
            }
            return Redirect("~/Home/Index");
        }

    }
}
