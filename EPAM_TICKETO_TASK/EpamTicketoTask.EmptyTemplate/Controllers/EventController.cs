﻿using EpamTicketoTask.BusinessLogic.Abstractions;
using EpamTicketoTask.BusinessLogic.Infrastucture;
using EpamTicketoTask.BusinessLogic.Models.DTO;
using EpamTicketoTask.EmptyTemplate.Models.ViewModels.Event;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EpamTicketoTask.EmptyTemplate.Controllers
{
    public class EventController : Controller
    {
        private IHostingEnvironment environment;
        private IServiceStorage serviceStorage;

        public EventController(IHostingEnvironment environment, IServiceStorage serviceStorage)
        {
            this.environment = environment;
            this.serviceStorage = serviceStorage;
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public IActionResult Create()
        {
            EventEditViewModel model = new EventEditViewModel()
            {
                VenueList = GetVenuesList()
            };
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public IActionResult Create(EventEditViewModel model)
        {
            if (!ModelState.IsValid)
            {
                model.VenueList = GetVenuesList();
                return View(model);
            }

            string photoPath;
            try
            {
                photoPath = ImageUploadAsync(model.Photo).Result;
            }
            catch (IOException)
            {
                ModelState.AddModelError("Photo", "Error while loading photo, try again");
                return View(model);
            }
            serviceStorage.GetService<IEventService>().Make(new EventDTO
            {
                Name = model.Name,
                Photo = photoPath,
                VenueId = model.VenueId,
                Date = model.Date,
                Description = model.Description,
            });
            return Redirect("~/Home/Index");

        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public IActionResult Edit(string eventId)
        {
            EventDTO dtoModel = serviceStorage.GetService<IEventService>().Get(eventId);

            EventEditViewModel model = new EventEditViewModel()
            {
                Id = dtoModel.Id,
                Name = dtoModel.Name,
                VenueId = dtoModel.VenueId,
                Date = dtoModel.Date,
                Description = dtoModel.Description,
                VenueList = GetVenuesList(),
                OldPhoto = dtoModel.Photo
            };
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public IActionResult Edit(EventEditViewModel model)
        {

            if (!ModelState.IsValid)
            {
                model.VenueList = GetVenuesList();
                return View(model);
            }
            string photoPath;
            try
            {
                photoPath = ImageUploadAsync(model.Photo).Result;
            }
            catch (IOException)
            {
                ModelState.AddModelError("Photo", "Error while loading photo, try again");
                return View(model);
            }
            serviceStorage.GetService<IEventService>().Update(new EventDTO
            {
                Id = model.Id,
                Name = model.Name,
                Photo = (photoPath == string.Empty) ? model.OldPhoto : photoPath,
                VenueId = model.VenueId,
                Date = model.Date,
                Description = model.Description
            });
            list = null;
            return Redirect("~/Home/Index");

        }


        [HttpGet]
        [Authorize(Roles = "admin")]
        public IActionResult Delete(string eventId)
        {
            EventDTO dtoModel = serviceStorage.GetService<IEventService>().Get(eventId);
            EventDisplayViewModel model = new EventDisplayViewModel
            {
                Date = dtoModel.Date,
                Id = dtoModel.Id,
                Name = dtoModel.Name,
                Photo = dtoModel.Photo,
                VenueFull = $"{dtoModel.CityName}, {dtoModel.Address}"
            };
            return View(model);
        }
        [HttpPost]
        [Authorize(Roles = "admin")]
        [ActionName("Delete")]
        public IActionResult DeleteConfirm(EventDisplayViewModel model)
        {
            try
            {
                serviceStorage.GetService<IEventService>().Delete(model.Id);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError("ConEv", e.Message);
                return View(model);
            }
            return Redirect("~/Home/Index");
        }

        private async Task<string> ImageUploadAsync(IFormFile file)
        {
            if (file == null)
            {
                return string.Empty;
            }
            string fileName = string.Concat(DateTime.Now.ToString("yyyy-MM-dd HHmmtt"), file.FileName);
            try
            {
                using (var fileStream = new FileStream(Path.Combine(environment.WebRootPath, "images", "Events", fileName), FileMode.CreateNew))
                {
                    await file.CopyToAsync(fileStream);
                }
            }
            catch (IOException)
            {
                //IMPOSSSIBRU!!!!!
                throw new NotImplementedException("IMPOSSSIBLE!!!");
            }
            return fileName;
        }


        private IEnumerable<SelectListItem> list;
        private IEnumerable<SelectListItem> GetVenuesList()
        {
            if (list == null)
            {
                list = serviceStorage.GetService<IVenueService>().GetAll()
                    .Select(v => new SelectListItem
                    {
                        Value = v.Id,
                        Text =$"{v.Name} - {v.CityName}, {v.Address}"
                    });
            }
            return list;
        }
    }
}
