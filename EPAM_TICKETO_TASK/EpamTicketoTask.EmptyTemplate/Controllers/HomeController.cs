﻿using Microsoft.AspNetCore.Mvc;
using EpamTicketoTask.BusinessLogic.Abstractions;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;
using EpamTicketoTask.BusinessLogic.Models.DTO;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Http;
using System;
using EpamTicketoTask.EmptyTemplate.Models.ViewModels.Order;
using EpamTicketoTask.EmptyTemplate.Models.ViewModels.Ticket;
using EpamTicketoTask.EmptyTemplate.Models.ViewModels.Event;

namespace EpamTicketoTask.EmptyTemplate.Controllers
{
    public class HomeController : Controller
    {
        private IServiceStorage serviceStorage;

        public HomeController(IServiceStorage serviceStorage)
        {
            this.serviceStorage = serviceStorage;
        }
        public IActionResult Index()
        {
            IEnumerable<EventDTO> modelDto = serviceStorage.GetService<IEventService>().GetAll();

            Mapper.Initialize(cfg => cfg.CreateMap<EventDTO, EventDisplayViewModel>()
            .ForMember("VenueFull", o => o.MapFrom(src => src.Address)));
            var model = Mapper.Map<IEnumerable<EventDTO>, List<EventDisplayViewModel>>(modelDto);

            return View(model);
        }
        public IActionResult EventDetail(string eventId)
        {
            IEnumerable<TicketDTO> ticketsDto = serviceStorage.GetService<ITicketService>().GetForEvent(eventId);
            Mapper.Initialize(cfg => cfg.CreateMap<TicketDTO, TicketIndexViewModel>());
            var tickets = Mapper.Map<IEnumerable<TicketDTO>, List<TicketIndexViewModel>>(ticketsDto);

            var modelDto = serviceStorage.GetService<IEventService>().Get(eventId);

            Mapper.Initialize(cfg => cfg.CreateMap<EventDTO, EventDetailViewModel>()
            .ForMember("VenueFull", o => o.MapFrom(src => src.Address))
            .ForMember("Tickets", o => o.MapFrom(src => tickets))
            .ForMember("Description", o => o.MapFrom(src => new HtmlString(src.Description))));

            var model = Mapper.Map<EventDTO, EventDetailViewModel>(modelDto);

            return View(model);
        }
        [Authorize]
        public IActionResult MyTickets([FromQuery]int sort = 0)
        {
            var ticketsDto = serviceStorage.GetService<ITicketService>().GetForUser(User.Identity.Name);
            switch (sort)
            {
                case 1:
                    ticketsDto = ticketsDto.Where(t => !t.IsSold);
                    break;
                case 2:
                    ticketsDto = ticketsDto.Where(t => t.OrderCount != 0 && !t.IsSold);
                    break;
                case 3:
                    ticketsDto = ticketsDto.Where(t => t.IsSold);
                    break;

            }
            Mapper.Initialize(cfg => cfg.CreateMap<TicketDTO, TicketIndexViewModel>());
            IEnumerable<TicketIndexViewModel> model = Mapper.Map<IEnumerable<TicketDTO>, List<TicketIndexViewModel>>(ticketsDto);

            return View(model);
        }
        [Authorize]
        public IActionResult MyOrders()
        {
            var ordersDto = serviceStorage.GetService<IOrderService>().GetUserOrders(User.Identity.Name);

            Mapper.Initialize(cfg => cfg.CreateMap<OrderDTO, OrderIndexViewModel>());
            IEnumerable<OrderIndexViewModel> model = Mapper.Map<IEnumerable<OrderDTO>, List<OrderIndexViewModel>>(ordersDto);

            return View(model);
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }

    }
}
