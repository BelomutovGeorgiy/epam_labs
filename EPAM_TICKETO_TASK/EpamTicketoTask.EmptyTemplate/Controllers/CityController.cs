﻿using EpamTicketoTask.BusinessLogic.Abstractions;
using EpamTicketoTask.BusinessLogic.Infrastucture;
using EpamTicketoTask.BusinessLogic.Models.DTO;
using EpamTicketoTask.EmptyTemplate.Models.ViewModels.City;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace EpamTicketoTask.EmptyTemplate.Controllers
{
    public class CityController : Controller
    {
        private IServiceStorage serviceStorage;
        public CityController(IServiceStorage serviceStorage)
        {
            this.serviceStorage = serviceStorage;
        }

        public IActionResult Index()
        {
            IEnumerable<CityViewModel> model = serviceStorage.GetService<ICityService>().GetAll()
                 .Select(v => new CityViewModel
                 {
                     Name = v.Name,
                     Id = v.Id
                 });
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CityViewModel model)
        {
            if (!ModelState.IsValid)
                return View("Create", model);

            CityDTO dtoModel = new CityDTO
            {
                Name = model.Name
            };
            serviceStorage.GetService<ICityService>().Make(dtoModel);

            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(string cityId)
        {
            CityDTO dtoModel = serviceStorage.GetService<ICityService>().Get(cityId);
            CityViewModel model = new CityViewModel
            {
                Id = dtoModel.Id,
                Name = dtoModel.Name
            };

            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CityViewModel model)
        {
            if (!ModelState.IsValid)
                return View("Create", model);

            CityDTO dtoModel = new CityDTO
            {
                Id = model.Id,
                Name = model.Name
            };
            serviceStorage.GetService<ICityService>().Update(dtoModel);

            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize]
        public IActionResult Delete(string cityId, string name)
        {
            return PartialView(new CityViewModel { Id = cityId, Name = name });
        }
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(CityViewModel model)
        {
            try
            {
                serviceStorage.GetService<ICityService>().Delete(model.Id);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View(model);
            }
            return RedirectToAction("Index", "City");
        }

    }
}
