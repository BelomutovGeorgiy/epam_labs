﻿using EpamTicketoTask.BusinessLogic.Abstractions;
using EpamTicketoTask.BusinessLogic.Infrastucture;
using EpamTicketoTask.BusinessLogic.Models.DTO;
using EpamTicketoTask.EmptyTemplate.Models.ViewModels.Venue;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace EpamTicketoTask.EmptyTemplate.Controllers
{
    public class VenueController : Controller
    {
        private IServiceStorage serviceStorage;
        public VenueController(IServiceStorage serviceStorage)
        {
            this.serviceStorage = serviceStorage;
        }

        public IActionResult Index()
        {
            IEnumerable<VenueIndexViewModel> model = serviceStorage.GetService<IVenueService>().GetAll()
                 .Select(v => new VenueIndexViewModel
                 {
                     Address = v.Address,
                     CityName = v.CityName,
                     Name = v.Name,
                     Id = v.Id
                 });
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            IEnumerable<CityDTO> cities = serviceStorage.GetService<ICityService>().GetAll();
            VenueEditViewModel model = new VenueEditViewModel { CityList = GetCityList() };

            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VenueEditViewModel model)
        {
            model.CityList = GetCityList();
            if (!ModelState.IsValid)
                return View("Create", model);

            VenueDTO dtoModel = new VenueDTO
            {
                Address = model.Address,
                CityId = model.CityId,
                Name = model.Name
            };
            serviceStorage.GetService<IVenueService>().Make(dtoModel);
            list = null;
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(string venueId)
        {
            IEnumerable<CityDTO> cities = serviceStorage.GetService<ICityService>().GetAll();
            
            VenueDTO dtoModel = serviceStorage.GetService<IVenueService>().Get(venueId);
            VenueEditViewModel model = new VenueEditViewModel
            {
                Address = dtoModel.Address,
                CityId = dtoModel.CityId,
                Id = dtoModel.Id,
                Name = dtoModel.Name,
                CityList = GetCityList()
            };

            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(VenueEditViewModel model)
        {
            model.CityList = GetCityList();
            if (!ModelState.IsValid)
                return View("Create", model);

            VenueDTO dtoModel = new VenueDTO
            {
                Id = model.Id,
                Address = model.Address,
                CityId = model.CityId,
                Name = model.Name
            };
            serviceStorage.GetService<IVenueService>().Update(dtoModel);
            list = null;
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize]
        public IActionResult Delete(string venueId, string name)
        {
            return PartialView(new VenueIndexViewModel { Id = venueId,  Name=name});
        }
        [HttpPost]
        [Authorize]
        public IActionResult Delete(VenueIndexViewModel model)
        {
            try
            {
                serviceStorage.GetService<IVenueService>().Delete(model.Id);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View(model);
            }
            return RedirectToAction("Index", "Venue");
        }



        private IEnumerable<SelectListItem> list;
        private IEnumerable<SelectListItem> GetCityList()
        {
            if (list == null)
            {
                list = serviceStorage.GetService<ICityService>().GetAll()
                    .Select(v => new SelectListItem
                    {
                        Value = v.Id,
                        Text = v.Name
                    });
            }
            return list;
        }
    }
}
