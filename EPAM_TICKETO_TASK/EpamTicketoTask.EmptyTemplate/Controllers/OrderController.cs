﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using EpamTicketoTask.BusinessLogic.Abstractions;
using Microsoft.AspNetCore.Authorization;
using EpamTicketoTask.BusinessLogic.Models.DTO;
using EpamTicketoTask.EmptyTemplate.Models.ViewModels;
using AutoMapper;
using EpamTicketoTask.EmptyTemplate.Models.ViewModels.Order;
using EpamTicketoTask.BusinessLogic.Infrastucture;
using EpamTicketoTask.EmptyTemplate.Models.ViewModels.Ticket;


namespace EpamTicketoTask.EmptyTemplate.Controllers
{
    public class OrderController : Controller
    {
        private IServiceStorage serviceStorage;

        public OrderController(IServiceStorage serviceStorage)
        {
            this.serviceStorage = serviceStorage;
        }



        [HttpGet]
        [Authorize]
        public IActionResult Add(string ticketId)
        {
            TicketDTO modelDto = serviceStorage.GetService<ITicketService>().Get(ticketId);

            Mapper.Initialize(cfg => cfg.CreateMap<TicketDTO, TicketIndexViewModel>());
            TicketIndexViewModel model = Mapper.Map<TicketDTO, TicketIndexViewModel>(modelDto);

            return View(model);
        }
        [HttpPost]
        [Authorize]
        [ActionName("Add")]
        public IActionResult AddConfirm(TicketIndexViewModel model)
        {
            try
            {
                serviceStorage.GetService<IOrderService>().Make(new OrderDTO
                {
                    BuyerUserName = User.Identity.Name,
                    TicketId = model.Id
                });
            }
            catch(ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View(model);
            }
            return Redirect("~/Home/MyOrders");
        }

        [HttpGet]
        [Authorize]
        [ActionName("Management")]
        public IActionResult OrderManagement(string ticketId)
        {

            
            IEnumerable<OrderDTO> modelDto = serviceStorage.GetService<IOrderService>().GetTicketOrders(ticketId);

            Mapper.Initialize(cfg => cfg.CreateMap<OrderDTO, OrderManagementListViewModel>());
            IEnumerable<OrderManagementListViewModel> orders = Mapper.Map<IEnumerable<OrderDTO>, List<OrderManagementListViewModel>>(modelDto);

            //И вот не знаю, два запроса это адекватно?
            TicketDTO ticketDto = serviceStorage.GetService<ITicketService>().Get(ticketId);

            Mapper.Initialize(cfg => cfg.CreateMap<TicketDTO, TicketIndexViewModel>());
            TicketIndexViewModel ticket = Mapper.Map<TicketDTO, TicketIndexViewModel>(ticketDto);


            OrderManagementViewModel model = new OrderManagementViewModel
            {
                Orders = orders,
                Ticket=ticket
            };

            return View(model);
        }


        [HttpGet]
        [Authorize]
        public IActionResult Confirm(string id, string ticketId)
        {
            return PartialView(new ConfirmViewModel { Id = id });
        }
        [HttpGet]
        [Authorize]
        public IActionResult Reject(string id, string ticketId)
        {
            return PartialView(new RejectViewModel { Id = id, TicketId=ticketId });
        }
        [HttpPost]
        [Authorize]
        public IActionResult Confirm(ConfirmViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            serviceStorage.GetService<IOrderService>().ConfirmOrder(model.Id, model.TrackNo);
            return RedirectToAction("MyTickets", "Home");
        }
        [HttpPost]
        [Authorize]
        public IActionResult Reject(RejectViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            serviceStorage.GetService<IOrderService>().DeclineOrder(model.Id, model.Comment);
            return Redirect($"~/Order/Management?ticketId={model.TicketId}");
        }


        [HttpGet]
        [Authorize]
        [ActionName("DeletePartial")]
        public IActionResult Delete(string orderId, string eventName)
        { 
            return PartialView(new OrderDeleteViewModel { Id=orderId, EventName=eventName});
        }
        [HttpPost]
        [Authorize]
        [ActionName("DeletePartial")]
        public IActionResult DeleteConfirm(OrderDeleteViewModel model)
        {
            try
            {
                serviceStorage.GetService<IOrderService>().Delete(model.Id);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View(model.Id);
            }
            return RedirectToAction("MyOrders","Home");
        }
    }
}
