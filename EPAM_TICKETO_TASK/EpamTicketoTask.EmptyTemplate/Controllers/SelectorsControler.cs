﻿using EpamTicketoTask.BusinessLogic.Abstractions;
using EpamTicketoTask.EmptyTemplate.Models.ViewModels.Venue;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace EpamTicketoTask.EmptyTemplate.Controllers
{
    public class SelectorsControler : Controller
    {
        private IServiceStorage serviceStorage;

        public SelectorsControler(IServiceStorage serviceStorage)
        {
            this.serviceStorage = serviceStorage;
        }
        [HttpGet]
        public IActionResult VenueSelect()
        {
            IEnumerable<SelectListItem> venueList= serviceStorage.GetService<IVenueService>().GetAll()
                .Select(v=>new SelectListItem
                { Value = v.Id,
                Text=v.Name
                });
            return PartialView();
        }
        [HttpPost]
        public IActionResult VenueSelect(VenueViewModel model)
        {
            if(!ModelState.IsValid)
            {
                return PartialView(model);
            }
            IEnumerable<SelectListItem> venueList = serviceStorage.GetService<IVenueService>().GetAll()
                .Select(v => new SelectListItem
                {
                    Value = v.Id,
                    Text = v.Name
                });
            return View();
        }
    }
}
