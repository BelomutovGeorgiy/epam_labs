﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using System.Globalization;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using EpamTicketoTask.BusinessLogic.Abstractions;
using EpamTicketoTask.BusinessLogic.Models.DTO;
using AutoMapper;
using EpamTicketoTask.BusinessLogic.Infrastucture;
using EpamTicketoTask.EmptyTemplate.Models.ViewModels.Auth;

namespace EpamTicketoTask.EmptyTemplate.Controllers
{
    public class AuthController : Controller
    {
        private IHostingEnvironment environment;
        private IServiceStorage serviceStorage;
        public AuthController(IServiceStorage serviceStorage, IHostingEnvironment environment)
        {
            this.environment = environment;
            this.serviceStorage = serviceStorage;
        }


        [HttpGet]
        public IActionResult Registration()
        {
            return View(new RegistrationViewModel());
        }
        [HttpPost]
        public IActionResult Registration(RegistrationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            UserDTO user = new UserDTO
            {
                UserName = model.UserName,
                Password = model.Password,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Localization = new CultureInfo(model.Localization)
            };
            try
            {
                serviceStorage.GetService<IUserService>().Make(user);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View(model);
            }
            serviceStorage.GetService<IUserService>().SignIn(model.UserName, model.Password, false);
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult Login([FromQuery] string returnUrl = "~/Home/Index")
        {
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        public IActionResult Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                serviceStorage.GetService<IUserService>().SignIn(model.Username, model.Password, model.RememberMe);
            }
            catch (ValidationException e)
            {
                ModelState.AddModelError(e.Property, e.Message);
                return View(model);
            }
            return Redirect(model.ReturnUrl);
        }

        [HttpGet]
        [Authorize(Roles = "admin, user")]//Finally it works
        public IActionResult Profile(string userName)
        {
            UserDTO user;
            if (userName == null)
                user = serviceStorage.GetService<IUserService>().GetByName(User.Identity.Name);
            else
                user = serviceStorage.GetService<IUserService>().GetByName(userName);

            Mapper.Initialize(cfg => cfg.CreateMap<UserDTO, UserDisplayViewModel>());

            UserDisplayViewModel model = Mapper.Map<UserDTO, UserDisplayViewModel>(user);
            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = "admin, user")]//Finally it works
        [ActionName("ProfileEdit")]
        public IActionResult EditProfile()
        {
            UserDTO user = serviceStorage.GetService<IUserService>().GetByName(User.Identity.Name);
            UserEditViewModel userModel = new UserEditViewModel
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Localization = user.Localization.TwoLetterISOLanguageName,
                Address = user.Address,
                Phone = user.PhoneNumber
            };
            return View(userModel);
        }

        [HttpPost]
        [Authorize(Roles = "admin, user")]
        [ActionName("ProfileEdit")]
        public IActionResult EditProfile(UserEditViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            string photoPath;
            try
            {
                photoPath=ImageUploadAsync(model.Photo).Result;
            }
            catch (IOException)
            {
                ModelState.AddModelError("Photo","Error while loading photo, try again");
                return View(model);
            }

            serviceStorage.GetService<IUserService>().Update(new UserDTO
            {
                UserName = User.Identity.Name,
                Photo = photoPath.Equals(string.Empty) ? model.PhotoPath : photoPath,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Localization = new CultureInfo(model.Localization),
                Address = model.Address,
                PhoneNumber = model.Phone
            });
            return RedirectToAction("Profile");
        }


        [HttpGet]
        [Authorize(Roles = "admin")]
        [ActionName("AdminPanel")]
        public IActionResult AdminPanel()
        {
            var usersDto = serviceStorage.GetService<IUserService>().GetAll().Where(u => u.UserName != User.Identity.Name);
            var rolesDto = serviceStorage.GetService<IRoleService>().GetAll();
            var roles = rolesDto.Select(r => r.Name);
            IEnumerable<UserRoleUpdateViewModel> users = usersDto.Select(
                 u => new UserRoleUpdateViewModel()
                 {
                     UserName = u.UserName,
                     FirstName = u.FirstName,
                     LastName = u.LastName,
                     Photo = u.Photo,
                     AllRoles = roles
                     //ОООчень стыдно за такой запрос но нв другое нет времени
                     //RolesToAssign = await userManager.GetRolesAsync(u)
                     //не стыдно, я уже и не помню что тут было

                 }).ToList();

            foreach (var u in users)
            {
                u.RolesToAssign = serviceStorage
                    .GetService<IUserService>()
                    .GetRoles(u.UserName);
            }
            AdminPanelViewModel model = new AdminPanelViewModel
            {
                UserList = users
            };
            return View(model);
        }


        [HttpPost]
        [ActionName("RoleCheckerPartial")]
        public IActionResult RoleCheckerPartial(UserRoleUpdateViewModel model, string returnUrl)
        {
            serviceStorage.GetService<IUserService>().AssignRolesAsync(model.UserName, model.RolesToAssign).Wait();

            return LocalRedirect(returnUrl);
        }
        [Authorize]
        public IActionResult LogOut()
        {
            serviceStorage.GetService<IUserService>().SignOut();
            return RedirectToAction("Index", "Home");
        }

        private async Task<string> ImageUploadAsync(IFormFile file)
        {
            if (file == null)
            {
                return string.Empty;
            }
            string fileName = string.Concat(DateTime.Now.ToString("yyyy-MM-dd HHmmttss"), file.FileName);
            try
            {
                using (var fileStream = new FileStream(Path.Combine(environment.WebRootPath, "images", "Users", fileName), FileMode.CreateNew))
                {
                    await file.CopyToAsync(fileStream);
                }
            }
            catch (IOException e)
            {
              throw e;
            }
            return fileName;
        }
    }
}
