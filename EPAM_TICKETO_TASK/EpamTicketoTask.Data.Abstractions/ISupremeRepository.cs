﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EpamTicketoTask.Data.Abstractions
{
    public interface ISupremeRepository
    {
        void SetStorageContext(IStorageContext storageContext);
    }
}
