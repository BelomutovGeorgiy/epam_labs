﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EpamTicketoTask.Data.Abstractions
{
    /// <summary>
    /// Access point to the Storage
    /// </summary>
    public interface IStorage
    {
        /// <summary>
        /// Find and returns repository of T
        /// </summary>
        /// <typeparam name="T">Type of desired model</typeparam>
        /// <returns>Repository for selected model</returns>
        T GetRepository<T>() where T : ISupremeRepository;
        /// <summary>
        /// Fix changes for all Repositories
        /// </summary>
        void Save();

        IStorageContext GetContext();
    }
}
