﻿using EpamTicketoTask.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EpamTicketoTask.Data.Abstractions.EntityRepositories
{
    public interface IOrderRepository : IRepository<Order>
    {
        IEnumerable<Order> OrdersOfUser(string userId);
    }
}
