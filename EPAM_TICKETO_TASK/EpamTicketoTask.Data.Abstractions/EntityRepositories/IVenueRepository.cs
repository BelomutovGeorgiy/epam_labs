﻿using EpamTicketoTask.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EpamTicketoTask.Data.Abstractions.EntityRepositories
{
    public interface IVenueRepository : IRepository<Venue>
    {
        Venue GetByName(string name);
    }
}
