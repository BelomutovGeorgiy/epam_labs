﻿using EpamTicketoTask.Data.Models;
using System;
using System.Collections.Generic;

namespace EpamTicketoTask.Data.Abstractions.EntityRepositories
{
   
    public interface IEventRepository : IRepository<Event>
    {
    }
}
