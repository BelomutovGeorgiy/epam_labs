﻿using System;
using System.Collections.Generic;
using System.Text;
using EpamTicketoTask.Data.Models;
using System.Linq;

namespace EpamTicketoTask.Data.Abstractions.EntityRepositories
{
    public interface ITicketRepository : IRepository<Ticket>
    {
        IQueryable<Ticket> TicketsOfUser(string userId);
        /// <summary>
        /// Get collection of tickets of selected user filtred by argument
        /// </summary>
        /// <param name="UserId">Id of user</param>
        /// <param name="SortArgument">Argument of filtering
        /// 0- all tickets
        /// 1- Selling
        /// 2- Waiting confirmation
        /// 3- Sold tickets
        /// </param>
        /// <returns>List of filtred tickets</returns>
        IEnumerable<Ticket> TicketsOfUser(string userId,int sortArgument);
        IEnumerable<Ticket> SellingTickets();
        IEnumerable<Ticket> WaitingTickets();
        IEnumerable<Ticket> SoldTickets();


    }
}
