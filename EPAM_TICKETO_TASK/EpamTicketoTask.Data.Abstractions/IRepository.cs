﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EpamTicketoTask.Data.Abstractions
{
    /// <summary>
    /// Describe repositories
    /// </summary>
    public interface IRepository<T> : ISupremeRepository where T : class 
    {
        /// <summary>
        /// send united storage context to repository
        /// </summary>
        /// <param name="storageContext">storage context to send</param>
        

        IQueryable<T> All();
        T Get(string id);
        void Create(T item);
        void Update(T item);
        void Delete(string id);
    }
}
