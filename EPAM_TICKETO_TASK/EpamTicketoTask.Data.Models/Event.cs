﻿using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Html;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Net;

namespace EpamTicketoTask.Data.Models
{
    public class Event
    {
        public Event(string id, string name, DateTime date, Venue venue, string photo, string description)
        {
            Id = id;
            Name = name;
            Date = date;
            Venue = venue;
            Photo = photo;
            Description = description;
        }
        public Event(string name, DateTime date, Venue venue, string photo, string description)
        {
            Name = name;
            Date = date;
            Venue = venue;
            Photo = photo;
            Description = description;
        }
        public Event() { }

        public string Id { get; set; }
        public string Name { get; set; }
        [Column(TypeName = "DateTime2")]
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public Venue Venue { get; set; }
        /// <summary>
        /// Path to photo of this event
        /// </summary>
        public string Photo { get; set; }
        /// <summary>
        /// Description of event in HTML form
        /// </summary>
        public List<Ticket> Tickets { get; set; } = new List<Ticket>();


    }
}
