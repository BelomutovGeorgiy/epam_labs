﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EpamTicketoTask.Data.Models
{
    public enum OrderStatuses
    {
        WaitingConfirmation,
        Rejected,
        Confirmed
    }
}
