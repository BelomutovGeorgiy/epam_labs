﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EpamTicketoTask.Data.Models
{
    public class Order
    {
        public Order(string id, Ticket ticket, OrderStatuses status, User buyer)
        {
            Id = id;
            Ticket = ticket;
            Status = status;
            Buyer = buyer;
        }
        public Order(Ticket ticket, OrderStatuses status, User buyer)
        {
            Ticket = ticket;
            Status = status;
            Buyer = buyer;
        }
        public Order(Ticket ticket, OrderStatuses status, User buyer, string trackNO,string comment)
        {
            Ticket = ticket;
            Status = status;
            Buyer = buyer;
            TrackNO = trackNO;
            Comment = comment;
        }
        [Key]
        public string Id { get; set; }
        public Ticket Ticket { get; set; }
        public OrderStatuses Status { get; set; }
        public User Buyer { get; set; }
        public string TrackNO { get; set; }
        public string Comment { get; set; }
        public Order() { }
    }
}
