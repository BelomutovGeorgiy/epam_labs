﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EpamTicketoTask.Data.Models
{
    public class City
    {
        public City(string id, string name)
        {
            Id = id;
            Name = name;
        }
        public City(string name)
        {
            Name = name;
        }

        public   string Id { get; set; }
        public   string Name { get; set; }

        public List<Venue> Venues { get; set; } = new List<Venue>();

        public City() { }
    }
}
