﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EpamTicketoTask.Data.Models
{
    public class Venue
    {
        public Venue(string id, string name, string address, City city)
        {
            Id = id;
            Name = name;
            Address = address;
            City = city;
        }
        public Venue(string name, string address, City city)
        {
            Name = name;
            Address = address;
            City = city;
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public City City { get; set; }
        public List<Event> Events { get; set; } = new List<Event>();

        public Venue() { }
    }
}
