﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EpamTicketoTask.Data.Models
{
    public class Ticket
    {
        public Ticket(string id, Event @event, decimal price, User seller)
        {
            Id = id;
            Event = @event;
            Price = price;
            Seller = seller;
        }
        public Ticket(Event @event, decimal price, User seller, string notes)
        {
            Event = @event;
            Price = price;
            Seller = seller;
            Notes = notes;
        }
        [Key]
        public string Id { get; set; }
        public Event Event { get; set; }
        public decimal Price { get; set; }
        public User Seller { get; set; }

        public string Notes { get; set; }

        public List<Order> Orders { get; set; } = new List<Order>();

        public Ticket() { }
    }
}
