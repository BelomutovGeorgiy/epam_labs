﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace EpamTicketoTask.Data.Models
{
    public class User : IdentityUser
    {
        public User()
        {
        }
        public User(string username, string password, string firstName, string lastName, CultureInfo culture) : base(username)
        {
           // Id = id;
            PasswordHash = password;
            Localization = culture;
            FirstName = firstName;
            LastName = lastName;
        }
        public User(string username, string password, string firstName, string lastName, CultureInfo localization, string address, string phoneNumber) : base(username)
        {
           // Id = id;
            PasswordHash = password;
            FirstName = firstName;
            LastName = lastName;
            Localization = localization;
            Address = address;
            PhoneNumber = phoneNumber;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Photo { get; set; }

        public CultureInfo Localization { get; set; }
        public string LocalizationISO
        {
            get { return Localization != null ? Localization.TwoLetterISOLanguageName : null; }
            set { Localization = new CultureInfo(value); }
        }
        
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}
