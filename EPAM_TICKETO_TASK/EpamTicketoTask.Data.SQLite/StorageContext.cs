﻿using EpamTicketoTask.Data.Abstractions;
using EpamTicketoTask.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace EpamTicketoTask.Data.SQLite
{
    public class StorageContext : IdentityDbContext<User, UserRole, string>, IStorageContext
    {
        private string connectionString = "Server=(localdb)\\mssqllocaldb;Database=ResaleDB_Belomutov_George;Trusted_Connection=True;";


        public StorageContext()
        {
        }
        public StorageContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(connectionString, b => b.MigrationsAssembly("EpamTicketoTask.EmptyTemplate"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<City>().ToTable("Cities");
            modelBuilder.Entity<Venue>().ToTable("Venues");
            modelBuilder.Entity<Event>().ToTable("Events");
            modelBuilder.Entity<Ticket>().ToTable("Tickets");
            modelBuilder.Entity<Order>().ToTable("Orders");

            modelBuilder.Entity<User>()
                .Ignore(u => u.Localization);
            modelBuilder.Entity<User>()
                .Property(u => u.LocalizationISO)
                .HasColumnName("Localization");
        }
    }
}
