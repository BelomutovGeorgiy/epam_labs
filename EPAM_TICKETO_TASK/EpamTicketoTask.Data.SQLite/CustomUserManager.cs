﻿using EpamTicketoTask.Data.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using System.Globalization;
using System.Security.Claims;
using System.Linq;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace EpamTicketoTask.Data.SQLite
{
    public class CustomUserManager : UserManager<User>
    {
        public CustomUserManager(IUserStore<User> store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<User> passwordHasher, IEnumerable<IUserValidator<User>> userValidators, IEnumerable<IPasswordValidator<User>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<User>> logger)
            : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
        }

        /// <summary>
        /// Check password for this user, without any encryption
        /// </summary>
        /// <param name="user">user, that password is need to check</param>
        /// <param name="Password">password needed to check</param>
        /// <returns>Task<bool> is it valid password</returns>
        public override Task<bool> CheckPasswordAsync(User user, string Password) =>
            Task.FromResult(user.PasswordHash == Password);
    }
}
