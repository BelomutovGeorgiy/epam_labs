﻿using EpamTicketoTask.Data.Abstractions;
using EpamTicketoTask.Data.SQLite.EntityRepositories;
using System;
using System.Reflection;

namespace EpamTicketoTask.Data.SQLite
{
    public class Storage : IStorage
    {
        public StorageContext StorageContext { get; private set; }

        private CityRepository cityRepository;
        private EventRepository eventRepository;
        private OrderRepository orderRepository;
        private TicketRepository ticketRepository;
        private VenueRepository venueRepository;


        public Storage()
        {
            StorageContext = new StorageContext("Server=(localdb)\\mssqllocaldb;Database=ResaleDB_Belomutov_George;Trusted_Connection=True;");
            //UserManager = new UserManager<User>(new UserStore<User>(StorageContext));
        }
        public T GetRepository<T>() where T : ISupremeRepository
        {
            foreach (FieldInfo t in this.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic))
            {
                if (typeof(T).GetTypeInfo().IsAssignableFrom(t.FieldType) && t.GetType().GetTypeInfo().IsClass)
                {
                    T repo;
                    object value = t.GetValue(this);
                    if (value == null)
                    {
                        repo = (T)Activator.CreateInstance(t.FieldType);
                        repo.SetStorageContext(StorageContext);
                    }
                    else
                        repo = (T)value;
                   
                    return repo;
                }
            }
            return default(T);
        }

        public void Save()
        {
            this.StorageContext.SaveChangesAsync().Wait();
        }

        public IStorageContext GetContext()
        {
            return StorageContext;
        }
    }
}
