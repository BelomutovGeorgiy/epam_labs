﻿using EpamTicketoTask.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EpamTicketoTask.Data.SQLite
{
    public static class MyQueryableExtentions
    {
        public static IQueryable<Venue> IncludeForVenue(this IQueryable<Venue> source) =>
            source.Include(v => v.City);

        public static IQueryable<Order> IncludeForOrder(this IQueryable<Order> source) =>
            source.Include(v => v.Buyer)
                  .Include(v => v.Ticket.Event.Venue.City)
                  .Include(v => v.Ticket.Seller);
        public static IQueryable<Ticket> IncludeForTicket(this IQueryable<Ticket> source) =>
            source.Include("Orders.Buyer")
                  .Include(v => v.Seller)
                  .Include(v => v.Event.Venue.City);
        public static IQueryable<Event> IncludeForEvent(this IQueryable<Event> source) =>
            source.Include(v => v.Venue.City)
                  .Include("Tickets.Seller")
                  .Include("Tickets.Orders");
        public static IQueryable<City> IncludeForCity(this IQueryable<City> source) =>
            source.Include(v => v.Venues);
    }
}
