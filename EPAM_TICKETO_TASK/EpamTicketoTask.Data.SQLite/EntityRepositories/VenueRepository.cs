﻿using EpamTicketoTask.Data.Abstractions.EntityRepositories;
using System;
using System.Collections.Generic;
using System.Text;
using EpamTicketoTask.Data.Abstractions;
using EpamTicketoTask.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace EpamTicketoTask.Data.SQLite.EntityRepositories
{
    class VenueRepository : IVenueRepository
    {
        private StorageContext storageContext;
        private DbSet<Venue> venues;

        public void SetStorageContext(IStorageContext storageContext)
        {
            this.storageContext = storageContext as StorageContext;
            this.venues = this.storageContext.Set<Venue>();
        }

        public IQueryable<Venue> All() => venues;

        public Venue Get(string id) => 
            venues.Where(x=>x.Id==id).Include(x=>x.City).FirstOrDefault();

        public void Create(Venue item)
        {
            venues.Add(item);
        }

        public void Update(Venue item)
        {
            venues.Update(item);
        }

        public void Delete(string id)
        {
            Venue venue = new Venue { Id = id };
            venues.Attach(venue);
            try
            {
                venues.Remove(venue);
                storageContext.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                throw e;
            }
        }

        public Venue GetByName(string name) => 
            venues.FirstOrDefault(v => v.Name == name);
    }
}
