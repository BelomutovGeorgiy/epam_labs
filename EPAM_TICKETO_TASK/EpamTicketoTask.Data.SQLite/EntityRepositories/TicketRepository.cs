﻿using EpamTicketoTask.Data.Abstractions.EntityRepositories;
using System;
using System.Collections.Generic;
using System.Text;
using EpamTicketoTask.Data.Abstractions;
using EpamTicketoTask.Data.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace EpamTicketoTask.Data.SQLite.EntityRepositories
{
    class TicketRepository : ITicketRepository
    {
        private StorageContext storageContext;
        private DbSet<Ticket> tickets;

        public void SetStorageContext(IStorageContext storageContext)
        {
            this.storageContext = storageContext as StorageContext;
            this.tickets = this.storageContext.Set<Ticket>();
        }
        public IQueryable<Ticket> All() =>
            tickets;

        public Ticket Get(string id) =>
            tickets.Where(x => x.Id == id)
            .Include(t => t.Orders)
            .Include("Orders.Buyer")
            .Include(v => v.Seller)
            .Include(v => v.Event.Venue.City)
            .FirstOrDefault();

        public void Create(Ticket item)
        {
            storageContext.Entry(item).State = EntityState.Added;
        }

        public IQueryable<Ticket> TicketsOfUser(string userName) =>
            tickets.Where(t => t.Seller.UserName == userName)
            .Include("Orders.Buyer")
            .Include(v => v.Seller)
            .Include(v => v.Event.Venue.City);

        public IEnumerable<Ticket> TicketsOfUser(string userId, int SortArgument)
        {
            IQueryable<Ticket> userTickets = TicketsOfUser(userId);
            switch (SortArgument)
            {
                case 0:
                    return userTickets;
                case 1:
                    return (from t in userTickets
                            where t.Orders.Count == 0 ||
                                  t.Orders.All(o => o.Status == OrderStatuses.Rejected)
                            select t);
                case 2:
                    return (from t in userTickets
                            where t.Orders.Count != 0 &&
                                  t.Orders.All(o => o.Status != OrderStatuses.WaitingConfirmation)
                            select t);
                case 3:
                    return (from t in userTickets
                            where t.Orders.Count != 0 &&
                                  t.Orders.Any(o => o.Status != OrderStatuses.Confirmed)
                            select t);
                default: return null;

            }
        }

        public IEnumerable<Ticket> SellingTickets() => tickets.Where(x => x.Orders.Count == 0);
        public IEnumerable<Ticket> WaitingTickets() => tickets.Where(x => x.Orders.Count != 0);
        public IEnumerable<Ticket> SoldTickets() => tickets.Where(x => x.Orders.Find(y => y.Status == OrderStatuses.Confirmed) != null);

        public void Update(Ticket item)
        {
            tickets.Update(item);
        }

        public void Delete(string id)
        {
            Ticket ticket=new Ticket { Id = id };
            tickets.Attach(ticket);
            try
            {
                tickets.Remove(ticket);
                storageContext.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                throw e;
            }
        }
    }
}
