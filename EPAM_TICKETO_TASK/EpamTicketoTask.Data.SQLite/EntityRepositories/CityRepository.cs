﻿using EpamTicketoTask.Data.Abstractions.EntityRepositories;
using System;
using System.Collections.Generic;
using System.Text;
using EpamTicketoTask.Data.Abstractions;
using EpamTicketoTask.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace EpamTicketoTask.Data.SQLite.EntityRepositories
{
    class CityRepository : ICityRepository
    {
        private StorageContext storageContext;
        private DbSet<City> cities;

        public void SetStorageContext(IStorageContext storageContext)
        {
            this.storageContext = storageContext as StorageContext;
            this.cities = this.storageContext.Set<City>();
        }

        public IQueryable<City> All() => cities;

        public City Get(string id) => All().FirstOrDefault(x=>x.Id==id);

        public void Create(City item)
        {
            cities.Add(item);
        }

        public void Update(City item)
        {
            cities.Update(item);
        }

        public void Delete(string id)
        {
           City city=new City { Id=id};
            cities.Attach(city);
            try
            {
                cities.Remove(city);
                storageContext.SaveChanges();
            }
            catch(DbUpdateException e)
            {
                throw e;
            }
        }
    }
}
