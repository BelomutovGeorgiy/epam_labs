﻿using EpamTicketoTask.Data.Abstractions.EntityRepositories;
using System;
using System.Collections.Generic;
using System.Text;
using EpamTicketoTask.Data.Abstractions;
using EpamTicketoTask.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace EpamTicketoTask.Data.SQLite.EntityRepositories
{
    class OrderRepository : IOrderRepository
    {
        private StorageContext storageContext;
        private DbSet<Order> orders;

        public void SetStorageContext(IStorageContext storageContext)
        {
            this.storageContext = storageContext as StorageContext;
            this.orders = this.storageContext.Set<Order>();
        }
        public IQueryable<Order> All() =>
            orders
            .Include(v => v.Buyer)
            .Include(v => v.Ticket.Event.Venue.City)
            .Include(v => v.Ticket.Seller);

        public Order Get(string id) =>
            All().FirstOrDefault(x => x.Id == id);

        public void Create(Order item)
        {
            storageContext.Entry(item).State = EntityState.Added;
        }

        public IEnumerable<Order> OrdersOfUser(string userName) =>
           orders.Where(o => o.Buyer.UserName == userName)
            .Include(v => v.Buyer)
            .Include(v => v.Ticket.Event.Venue.City)
            .Include(v => v.Ticket.Seller);

        public void Update(Order item)
        {
            orders.Update(item);
        }

        public void Delete(string id)
        {
            //var ev = orders.Where(e => e.Id == id).FirstOrDefault();

            //Попробую удалить без запроса к бд
            //Лол, работает
            Order order = new Order { Id = id };
            orders.Attach(order);
            try
            {
               orders.Remove(order);
                storageContext.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                throw e;
            }
        }
    }
}
