﻿using EpamTicketoTask.Data.Abstractions.EntityRepositories;
using System;
using System.Collections.Generic;
using System.Text;
using EpamTicketoTask.Data.Abstractions;
using EpamTicketoTask.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace EpamTicketoTask.Data.SQLite.EntityRepositories
{
    class EventRepository : IEventRepository
    {
        private StorageContext storageContext;
        private DbSet<Event> events;

        public void SetStorageContext(IStorageContext storageContext)
        {
            this.storageContext = storageContext as StorageContext;
            this.events = this.storageContext.Set<Event>();
        }
        public IQueryable<Event> All() => events;

        public Event Get(string id)=>
            events
            .Where(x => x.Id == id)
            .Include(e => e.Venue.City)
            .Include("Tickets.Seller")
            .Include("Tickets.Orders")
            .FirstOrDefault();

        public void Create(Event item)
        {
            events.Add(item);
        }

        public void Delete(string id)
        {
            var ev=events.Where(e => e.Id == id).FirstOrDefault();
            try
            {
                events.Remove(ev);
                storageContext.SaveChanges();
            }
            catch(DbUpdateException e)
            {
                throw e;
            }
        }

        public void Update(Event item)
        {
            events.Update(item);
        }
    }
}
