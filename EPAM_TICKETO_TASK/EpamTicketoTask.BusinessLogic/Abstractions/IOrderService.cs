﻿using EpamTicketoTask.BusinessLogic.Models.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace EpamTicketoTask.BusinessLogic.Abstractions
{
    public interface IOrderService : ICrudService<OrderDTO>
    {
        IEnumerable<OrderDTO> GetUserOrders(string userName);
        IEnumerable<OrderDTO> GetTicketOrders(string ticketId);
        void ConfirmOrder(string id, string trackNO);
        void DeclineOrder(string id, string comment);
    }
}
