﻿using EpamTicketoTask.BusinessLogic.Models.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace EpamTicketoTask.BusinessLogic.Abstractions
{
    public interface IEventService: ICrudService<EventDTO>
    {
    }
}