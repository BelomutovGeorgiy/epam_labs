﻿using EpamTicketoTask.BusinessLogic.Abstractions.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EpamTicketoTask.BusinessLogic.Abstractions
{
    public interface ICrudService<T> : ISupremeService where T:class,IDtoModel
    {
        void Make(T dtoObject);
        void Update(T dtoObject);
        void Delete(string id);
        T Get(string id);
        IEnumerable<T> GetAll();
    }
}
