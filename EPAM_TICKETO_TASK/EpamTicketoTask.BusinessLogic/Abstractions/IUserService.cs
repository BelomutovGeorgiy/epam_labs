﻿using EpamTicketoTask.BusinessLogic.Models.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EpamTicketoTask.BusinessLogic.Abstractions
{
    public interface IUserService : ICrudService<UserDTO>
    {
        UserDTO GetByName(string userName);
        void SignIn(string userName, string password, bool remember);
        void SignOut();
        Task AssignRolesAsync(string userName, IEnumerable<string> roles);
        IEnumerable<string> GetRoles(string userName);
    }
}
