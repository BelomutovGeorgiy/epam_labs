﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EpamTicketoTask.BusinessLogic.Abstractions
{
    public interface IServiceStorage
    {
        T GetService<T>() where T : ISupremeService;

    }
}
