﻿using EpamTicketoTask.BusinessLogic.Models.DTO;

namespace EpamTicketoTask.BusinessLogic.Abstractions
{
    public interface ICityService : ICrudService<CityDTO>
    {

    }
}
