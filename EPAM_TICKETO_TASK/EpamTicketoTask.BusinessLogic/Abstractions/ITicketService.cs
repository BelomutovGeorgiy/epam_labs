﻿using EpamTicketoTask.BusinessLogic.Abstractions;
using EpamTicketoTask.BusinessLogic.Models.DTO;
using System.Collections.Generic;

namespace EpamTicketoTask.BusinessLogic.Abstractions
{
    public interface ITicketService : ICrudService<TicketDTO>
    {
        IEnumerable<TicketDTO> GetForEvent(string eventId);
        IEnumerable<TicketDTO> GetForUser(string userId);
    }
}
