﻿using EpamTicketoTask.BusinessLogic.Models.DTO;

namespace EpamTicketoTask.BusinessLogic.Abstractions
{
    public interface IRoleService : ICrudService<RoleDTO>
    {
    }
}
