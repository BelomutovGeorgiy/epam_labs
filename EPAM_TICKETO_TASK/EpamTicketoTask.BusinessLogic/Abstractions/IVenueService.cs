﻿using EpamTicketoTask.BusinessLogic.Models.DTO;

namespace EpamTicketoTask.BusinessLogic.Abstractions
{
    public interface IVenueService : ICrudService<VenueDTO>
    {
    }
}
