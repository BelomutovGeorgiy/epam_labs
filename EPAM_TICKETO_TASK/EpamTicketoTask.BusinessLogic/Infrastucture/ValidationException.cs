﻿using System;

namespace EpamTicketoTask.BusinessLogic.Infrastucture
{
    public class ValidationException : Exception
    {
        public string Property { get; }
        public ValidationException(string message, string property) : base(message)
        {
            Property = property;
        }
    }
}
