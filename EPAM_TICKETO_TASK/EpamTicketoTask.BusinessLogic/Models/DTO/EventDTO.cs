﻿using System;
using Microsoft.AspNetCore.Html;
using EpamTicketoTask.BusinessLogic.Abstractions.Models;

namespace EpamTicketoTask.BusinessLogic.Models.DTO
{
    public class EventDTO : IDtoModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string VenueId { get; set; }
        public string Address { get; set; }
        public string CityName { get; set; }
        public string Photo { get; set; }
        public string Description { get; set; }
    }
}
