﻿using EpamTicketoTask.BusinessLogic.Abstractions.Models;

namespace EpamTicketoTask.BusinessLogic.Models.DTO
{
    public class CityDTO : IDtoModel
    {
        public string Id;
        public string Name;
    }
}
