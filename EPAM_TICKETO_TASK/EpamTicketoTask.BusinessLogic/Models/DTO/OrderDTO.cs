﻿using EpamTicketoTask.BusinessLogic.Abstractions.Models;
using EpamTicketoTask.Data.Models;
using System;

namespace EpamTicketoTask.BusinessLogic.Models.DTO
{
    public class OrderDTO : IDtoModel
    {
        public string Id { get; set; }
        public string TicketId { get; set; }
        public string TicketEventId { get; set; }
        public string TicketEventName { get; set; }
        public OrderStatuses Status { get; set; }
        public string BuyerUserName { get; set; }
        public string BuyerFullName { get; set; }
        public string BuyerPhoto { get; set; }
        public string SellerUserName { get; set; }
        public string SellerFullName { get; set; }
        public decimal TicketPrice { get; set; }
        public string TicketNotes { get; set; }
        public string TrackNo { get; set; }
        public string Comment { get; set; }
    }
}
