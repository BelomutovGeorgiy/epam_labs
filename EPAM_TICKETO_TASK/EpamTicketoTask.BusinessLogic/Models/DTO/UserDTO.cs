﻿using EpamTicketoTask.BusinessLogic.Abstractions.Models;
using System.Globalization;

namespace EpamTicketoTask.BusinessLogic.Models.DTO
{
    public class UserDTO : IDtoModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Photo { get; set; }
        public CultureInfo Localization { get; set; }
        public string[] Roles { get; set; }
    }
}
