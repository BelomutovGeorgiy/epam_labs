﻿using EpamTicketoTask.BusinessLogic.Abstractions.Models;

namespace EpamTicketoTask.BusinessLogic.Models.DTO
{
    public class RoleDTO : IDtoModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
