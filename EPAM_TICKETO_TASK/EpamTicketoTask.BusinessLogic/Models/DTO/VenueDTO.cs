﻿using EpamTicketoTask.BusinessLogic.Abstractions.Models;

namespace EpamTicketoTask.BusinessLogic.Models.DTO
{
    public class VenueDTO : IDtoModel
    {
        public string Id;
        public string Name;
        public string Address;
        public string CityId;
        public string CityName;
    }
}
