﻿using EpamTicketoTask.BusinessLogic.Abstractions.Models;
using System;

namespace EpamTicketoTask.BusinessLogic.Models.DTO
{
    public class TicketDTO : IDtoModel
    {
        public string Id { get; set; }
        public string EventId { get; set; }
        public string EventName { get; set; }
        public string EventAddress { get; set; }
        public DateTime EventDate { get; set; }
        public string SellerUserName { get; set; }
        public string SellerFullName { get; set; }
        public string Notes { get; set; }
        public decimal Price { get; set; }
        public int OrderCount { get; set; }
        public bool IsSold { get; set; }
    }
}
