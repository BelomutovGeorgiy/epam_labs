﻿using EpamTicketoTask.BusinessLogic.Abstractions;
using EpamTicketoTask.Data.Abstractions;
using EpamTicketoTask.Data.Models;
using Microsoft.AspNetCore.Identity;

namespace EpamTicketoTask.BusinessLogic.Services
{
    public abstract class AbstractService
    {
        protected IStorage storage;
        protected UserManager<User> userManager;
        public AbstractService(ServiceStorage storage)
        {
            this.storage = storage.Storage;
            this.userManager = storage.UserManager;
        }
    }
}
