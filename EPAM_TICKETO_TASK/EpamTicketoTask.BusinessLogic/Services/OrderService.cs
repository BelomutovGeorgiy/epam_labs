﻿using System.Collections.Generic;
using EpamTicketoTask.BusinessLogic.Abstractions;
using EpamTicketoTask.BusinessLogic.Models.DTO;
using EpamTicketoTask.Data.Abstractions;
using EpamTicketoTask.Data.Models;
using EpamTicketoTask.Data.Abstractions.EntityRepositories;
using EpamTicketoTask.BusinessLogic.Infrastucture;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using AutoMapper;

namespace EpamTicketoTask.BusinessLogic.Services
{
    public class OrderService : AbstractService, IOrderService
    {

        private IOrderRepository orderRepository;

        public OrderService(ServiceStorage storage) : base(storage)
        {
            orderRepository = base.storage.GetRepository<IOrderRepository>();
        }

        public void ConfirmOrder(string id, string trackNO)
        {
            if (trackNO == String.Empty)
                throw new ValidationException("Track number is required", "trackNO");
            Order o = orderRepository.Get(id);
            o.TrackNO = trackNO;
            o.Status = OrderStatuses.Confirmed;

            IEnumerable<Order> rejectedOrders = storage.GetRepository<IOrderRepository>().All()
                .Where(x => x.Ticket.Id == o.Ticket.Id)
                .Where(x => x.Id != o.Id)
                .Where(x => x.Status == OrderStatuses.WaitingConfirmation);

            foreach (Order ord in rejectedOrders)
            {
                ord.Status = OrderStatuses.Rejected;
                ord.Comment = $"Lot going to {o.Buyer.FirstName} {o.Buyer.LastName}";
                orderRepository.Update(ord);
            }

            orderRepository.Update(o);
            storage.Save();
        }

        public void DeclineOrder(string id, string comment)
        {
            Order o = orderRepository.Get(id);
            o.Comment = comment;
            o.Status = OrderStatuses.Rejected;

            orderRepository.Update(o);
            storage.Save();
        }

        public void Delete(string id)
        {
            if (id == null)
                throw new ValidationException("Invalid id", "");
            try
            {
                orderRepository.Delete(id);
                storage.Save();
            }
            catch (DbUpdateException)
            {
                throw new ValidationException("Order has related", "");
            }
        }

        public OrderDTO Get(string id)
        {
            if (id == null)
                throw new ValidationException("Invalid id", "");
            Order o = orderRepository.Get(id);
            if (o == null)
                throw new ValidationException("Order Not Found", "");
            Mapper.Initialize(cfg => cfg.CreateMap<Order, OrderDTO>()
           .ForMember("BuyerFullName", opt => opt.MapFrom(src => $"{src.Buyer.FirstName} {src.Buyer.LastName}"))
           .ForMember("SellerFullName", opt => opt.MapFrom(src => $"{src.Ticket.Seller.FirstName} {src.Ticket.Seller.LastName}")));
            OrderDTO order = Mapper.Map<Order, OrderDTO>(o);
            return order;
        }

        public IEnumerable<OrderDTO> GetAll()
        {
            IEnumerable<Order> orders = orderRepository.All();

            Mapper.Initialize(cfg => cfg.CreateMap<Order, OrderDTO>()
           .ForMember("BuyerFullName", opt => opt.MapFrom(src => $"{src.Buyer.FirstName} {src.Buyer.LastName}"))
           .ForMember("SellerFullName", opt => opt.MapFrom(src => $"{src.Ticket.Seller.FirstName} {src.Ticket.Seller.LastName}")));
            IEnumerable<OrderDTO> dtoOrders = Mapper.Map<IEnumerable<Order>, List<OrderDTO>>(orders);
            return dtoOrders;
        }

        public IEnumerable<OrderDTO> GetTicketOrders(string ticketId)
        {
            IEnumerable<Order> orders = storage.GetRepository<IOrderRepository>().All().Where(o => o.Ticket.Id == ticketId);

            Mapper.Initialize(cfg => cfg.CreateMap<Order, OrderDTO>()
            .ForMember("BuyerFullName", opt => opt.MapFrom(src => $"{src.Buyer.FirstName} {src.Buyer.LastName}"))
           .ForMember("SellerFullName", opt => opt.MapFrom(src => $"{src.Ticket.Seller.FirstName} {src.Ticket.Seller.LastName}")));
            IEnumerable<OrderDTO> dtoOrders = Mapper.Map<IEnumerable<Order>, List<OrderDTO>>(orders);
            return dtoOrders;
        }

        public IEnumerable<OrderDTO> GetUserOrders(string userName)
        {
            IEnumerable<Order> orders = storage.GetRepository<IOrderRepository>().OrdersOfUser(userName);

            Mapper.Initialize(cfg => cfg.CreateMap<Order, OrderDTO>()
           .ForMember("BuyerFullName", opt => opt.MapFrom(src => $"{src.Buyer.FirstName} {src.Buyer.LastName}"))
           .ForMember("SellerFullName", opt => opt.MapFrom(src => $"{src.Ticket.Seller.FirstName} {src.Ticket.Seller.LastName}")));
            IEnumerable<OrderDTO> dtoOrders = Mapper.Map<IEnumerable<Order>, List<OrderDTO>>(orders);
            return dtoOrders;
        }

        public void Make(OrderDTO order)
        {
            Ticket ticket = storage.GetRepository<ITicketRepository>().Get(order.TicketId);
            User buyer = userManager.FindByNameAsync(order.BuyerUserName).Result;
            if (ticket == null)
                throw new ValidationException("Ticket Not Found", "");

            if (buyer == null)
                throw new ValidationException("Invalid Buyer", "");

            if (orderRepository.OrdersOfUser(order.BuyerUserName).Where(o => o.Ticket.Id == order.TicketId).Any())
                throw new ValidationException("You already ordered this ticket", "");

            Order resultOrder = new Order(ticket, OrderStatuses.WaitingConfirmation, buyer);

            orderRepository.Create(resultOrder);
            storage.Save();
        }

        public void Update(OrderDTO order)
        {
            if (order.Id == null)
                throw new ValidationException("Order Not Found", "");
            Order resultOrder = orderRepository.Get(order.Id);
            resultOrder.Status = order.Status;
            resultOrder.Comment = order.Comment;
            resultOrder.TrackNO = order.TrackNo;

            orderRepository.Update(resultOrder);
            storage.Save();
        }

    }
}
