﻿using EpamTicketoTask.BusinessLogic.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using EpamTicketoTask.BusinessLogic.Models.DTO;
using EpamTicketoTask.Data.Abstractions;
using EpamTicketoTask.Data.Models;
using Microsoft.AspNetCore.Identity;
using System.Linq;

namespace EpamTicketoTask.BusinessLogic.Services
{
    public class RoleService : IRoleService
    {
        private RoleManager<UserRole> roleManager;
        public RoleService(ServiceStorage storage)
        {
            roleManager = storage.RoleManager;
        }

        public void Delete(string id)
        {
            throw new NotImplementedException();
        }

        public RoleDTO Get(string id)
        {
            UserRole r = roleManager.FindByIdAsync(id).Result;
            return new RoleDTO { Id = r.Id, Name = r.Name };
        }

        public IEnumerable<RoleDTO> GetAll()
        {
            IEnumerable<RoleDTO> roles = roleManager.Roles
                .Select(r => new RoleDTO { Id = r.Id, Name = r.Name });
            return roles;
        }

        public void Make(RoleDTO dtoObject)
        {
            throw new NotImplementedException();
        }

        public void Update(RoleDTO dtoObject)
        {
            throw new NotImplementedException();
        }
    }
}
