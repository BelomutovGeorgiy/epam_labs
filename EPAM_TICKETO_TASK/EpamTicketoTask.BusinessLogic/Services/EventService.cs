﻿using System.Collections.Generic;
using EpamTicketoTask.Data.Abstractions;
using EpamTicketoTask.Data.Models;
using Microsoft.AspNetCore.Identity;
using EpamTicketoTask.BusinessLogic.Abstractions;
using EpamTicketoTask.BusinessLogic.Models.DTO;
using EpamTicketoTask.Data.Abstractions.EntityRepositories;
using EpamTicketoTask.BusinessLogic.Infrastucture;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace EpamTicketoTask.BusinessLogic.Services
{
    public class EventService : AbstractService, IEventService
    {
        private IEventRepository eventRepository;
        public EventService(ServiceStorage storage) : base(storage)
        {
            eventRepository=base.storage.GetRepository<IEventRepository>();
        }
       
        public void Delete(string id)
        {
            if (id == null)
                throw new ValidationException("Invalid id", "");
            try
            {
                eventRepository.Delete(id);
                storage.Save();
            }
            catch (DbUpdateException)
            {
                throw new ValidationException("Event has related", "");
            }
        }

        public EventDTO Get(string id)
        {
            if (id == null)
                throw new ValidationException("Invalid id", "");
            Event e = eventRepository.Get(id);
            if (e == null)
                throw new ValidationException("Event Not Found", "");
            EventDTO @event = new EventDTO
            {
                Id = e.Id,
                Address = $"{e.Venue.Name} ({e.Venue.Address})",
                CityName = e.Venue.City.Name,
                Date = e.Date,
                Description = e.Description,
                Name = e.Name,
                Photo = e.Photo,
                VenueId = e.Venue.Id
            };
            return @event;
        }

        public IEnumerable<EventDTO> GetAll()
        {
            IEnumerable<EventDTO> events = eventRepository.All()
                .Select(e=> new EventDTO
                {
                    Id = e.Id,
                    Address = $"{e.Venue.Name} ({e.Venue.Address})",
                    CityName = e.Venue.City.Name,
                    Date = e.Date,
                    Description = e.Description,
                    Name = e.Name,
                    Photo = e.Photo,
                    VenueId = e.Venue.Id
                });
            return events;
        }
        public void Make(EventDTO @event)
        {
            Venue venue = storage.GetRepository<IVenueRepository>().Get(@event.VenueId);
            if (venue == null)
                throw new ValidationException("Venue Not Found", "");
            Event resultEvent = new Event(@event.Name, @event.Date, venue, @event.Photo, @event.Description);

            storage.GetRepository<IEventRepository>().Create(resultEvent);
            storage.Save();
        }
        public void Update(EventDTO @event)
        {
            if (@event.Id == null)
                throw new ValidationException("Id is null", "");
            Venue venue = storage.GetRepository<IVenueRepository>().Get(@event.VenueId);
            if (venue == null)
                throw new ValidationException("Venue Not Found", "");
            Event resultEvent = new Event
            {
                Name = @event.Name,
                Date = @event.Date,
                Description = @event.Description,
                Id = @event.Id,
                Venue = venue
            };
            if (@event.Photo != string.Empty)
                resultEvent.Photo = @event.Photo;
            eventRepository.Update(resultEvent);
            storage.Save();
        }
    }
}
