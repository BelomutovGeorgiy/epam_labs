﻿using System;
using System.Collections.Generic;
using EpamTicketoTask.BusinessLogic.Models.DTO;
using EpamTicketoTask.Data.Abstractions;
using EpamTicketoTask.Data.Models;
using Microsoft.AspNetCore.Identity;
using EpamTicketoTask.Data.Abstractions.EntityRepositories;
using EpamTicketoTask.BusinessLogic.Infrastucture;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using EpamTicketoTask.BusinessLogic.Abstractions;
using AutoMapper;

namespace EpamTicketoTask.BusinessLogic.Services
{
    public class TicketService : AbstractService, ITicketService
    {
        public TicketService(ServiceStorage storage) : base(storage)
        {
        }

        public void Delete(string id)
        {
            if (id == null)
                throw new ValidationException("Invalid id", "");
            try
            {
                storage.GetRepository<ITicketRepository>().Delete(id);
                storage.Save();
            }
            catch (DbUpdateException)
            {
                throw new ValidationException("Ticket has relations", "");
            }
        }

        public TicketDTO Get(string id)
        {
            Ticket t = storage.GetRepository<ITicketRepository>().Get(id);
            if (t == null)
                throw new ValidationException("Ticket Not Found", "");
            TicketDTO ticket = new TicketDTO
            {
                Id = t.Id,
                EventId=t.Event.Id,
                EventName = t.Event.Name,
                EventAddress = $"{t.Event.Venue.Name} ({t.Event.Venue.City}, {t.Event.Venue.Address})",
                EventDate = t.Event.Date,
                Notes = t.Notes,
                Price = t.Price,
                SellerUserName = t.Seller.UserName,
                SellerFullName = $"{ t.Seller.FirstName } { t.Seller.LastName }",
                OrderCount = t.Orders.Count
            };
            return ticket;
        }

        public IEnumerable<TicketDTO> GetAll()
        {
            IEnumerable<TicketDTO> tickets = storage.GetRepository<ITicketRepository>().All()
               .Select(t => new TicketDTO
               {
                   Id = t.Id,
                   EventName = t.Event.Name,
                   EventAddress = $"{t.Event.Venue.Name} ({t.Event.Venue.City}, {t.Event.Venue.Address})",
                   EventDate = t.Event.Date,
                   Notes = t.Notes,
                   Price = t.Price,
                   SellerUserName = t.Seller.UserName,
                   SellerFullName = $"{ t.Seller.FirstName } { t.Seller.LastName }"
               });
            return tickets;
        }

        public IEnumerable<TicketDTO> GetForEvent(string eventId)
        {

            IEnumerable<Ticket> tickets = storage.GetRepository<IEventRepository>().Get(eventId).Tickets;

            Mapper.Initialize(cfg => cfg.CreateMap<Ticket, TicketDTO>()
            .ForMember("IsSold", o => o.MapFrom(t => t.Orders.Any(ord => ord.Status == OrderStatuses.Confirmed)))
            .ForMember("EventAddress", o => o.MapFrom(t => $"{t.Event.Venue.Name} ({t.Event.Venue.City}, {t.Event.Venue.Address})"))
            .ForMember("SellerFullName", o => o.MapFrom(t => $"{ t.Seller.FirstName } { t.Seller.LastName }"))
            .ForMember("OrderCount", o=>o.MapFrom(t=>t.Orders.Count)));

            var ticketsDto = Mapper.Map<IEnumerable<Ticket>, List<TicketDTO>>(tickets);

            return ticketsDto;
        }

        public IEnumerable<TicketDTO> GetForUser(string userName)
        {
            IEnumerable<Ticket> tickets = storage.GetRepository<ITicketRepository>().TicketsOfUser(userName);

            Mapper.Initialize(cfg => cfg.CreateMap<Ticket, TicketDTO>()
            .ForMember("IsSold", o=> o.MapFrom(t=>t.Orders.Any(ord => ord.Status == OrderStatuses.Confirmed)))
            .ForMember("EventAddress", o => o.MapFrom(t => $"{t.Event.Venue.Name} ({t.Event.Venue.City}, {t.Event.Venue.Address})"))
            .ForMember("SellerFullName", o => o.MapFrom(t => $"{ t.Seller.FirstName } { t.Seller.LastName }"))
            .ForMember("OrderCount", o => o.MapFrom(t => t.Orders.Count)));

            var ticketsDto = Mapper.Map<IEnumerable<Ticket>, List<TicketDTO>>(tickets);

            return ticketsDto;
        }

        public void Make(TicketDTO ticket)
        {
            Event @event = storage.GetRepository<IEventRepository>().Get(ticket.EventId);
            User seller = userManager.FindByNameAsync(ticket.SellerUserName).Result;
            if (@event == null)
                throw new ValidationException("Event Not Found", "");
            if (seller == null)
                throw new ValidationException("Invalid Buyer", "");
            Ticket resultTicket = new Ticket(@event, ticket.Price, seller,ticket.Notes);
            storage.GetRepository<ITicketRepository>().Create(resultTicket);
            storage.Save();
        }

        public void Update(TicketDTO dtoObject)
        {
            throw new NotImplementedException();
        }
    }
}
