﻿using EpamTicketoTask.BusinessLogic.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using EpamTicketoTask.Data.Abstractions;
using EpamTicketoTask.Data.Models;
using Microsoft.AspNetCore.Identity;
using EpamTicketoTask.BusinessLogic.Models.DTO;
using EpamTicketoTask.BusinessLogic.Infrastucture;
using EpamTicketoTask.Data.Abstractions.EntityRepositories;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace EpamTicketoTask.BusinessLogic.Services
{
    class VenueService : AbstractService, IVenueService
    {
        private IVenueRepository venueRepository;
        public VenueService(ServiceStorage storage) : base(storage)
        {
            venueRepository = base.storage.GetRepository<IVenueRepository>();
        }

        public void Delete(string id)
        {
            if (id == null)
                throw new ValidationException("Invalid id", "");
            try
            {
                storage.GetRepository<IVenueRepository>().Delete(id);
                storage.Save();
            }
            catch (DbUpdateException)
            {
                throw new ValidationException("Event has related", "");
            }
        }

        public VenueDTO Get(string id)
        {
            if (id == null)
                throw new ValidationException("Invalid id", "");
            Venue v = venueRepository.Get(id);
            if (v == null)
                throw new ValidationException("Venue Not Found", "");
            VenueDTO venue = new VenueDTO
            {
                Id = v.Id,
                Name = v.Name,
                Address = v.Address,
                CityId = v.City.Id,
                CityName = v.City.Name
            };
            return venue;
        }

        public IEnumerable<VenueDTO> GetAll()
        {
            IEnumerable<VenueDTO> venues = venueRepository.All()
                 .Select(v => new VenueDTO
                 {
                     Id = v.Id,
                     Name=v.Name,
                     Address = v.Address,
                     CityId = v.City.Id,
                     CityName = v.City.Name
                 });
            return venues;
        }

        public void Make(VenueDTO venue)
        {
            City city = storage.GetRepository<ICityRepository>().Get(venue.CityId);
            if (city == null)
                throw new ValidationException("City Not Found", "");
            Venue resultVenue = new Venue(venue.Name,venue.Address,city);
            venueRepository.Create(resultVenue);
            storage.Save();
        }

        public void Update(VenueDTO venue)
        {
            if (venue.Id == null)
                throw new ValidationException("Venue Not Found", "");
            City city = storage.GetRepository<ICityRepository>().Get(venue.CityId);
            if (city == null)
                throw new ValidationException("City Not Found", "");
            Venue resultVenue = new Venue(venue.Id,venue.Name,venue.Address,city);
            venueRepository.Update(resultVenue);
            storage.Save();
        }
    }
}
