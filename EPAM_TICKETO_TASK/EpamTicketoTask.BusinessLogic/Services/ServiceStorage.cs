﻿using System;
using EpamTicketoTask.BusinessLogic.Abstractions;
using System.Reflection;
using EpamTicketoTask.Data.Abstractions;
using Microsoft.AspNetCore.Identity;
using EpamTicketoTask.Data.Models;

namespace EpamTicketoTask.BusinessLogic.Services
{
    public class ServiceStorage : IServiceStorage
    {
        private CityService cityService;
        private EventService eventService;
        private OrderService orderService;
        private TicketService ticketService;
        private VenueService venueService;
        private UserService roleService;
        private RoleService userService;

        internal IStorage Storage;
        internal UserManager<User> UserManager;
        internal SignInManager<User> SignInManager;
        internal RoleManager<UserRole> RoleManager;
        public ServiceStorage(IStorage storage, UserManager<User> userManager, SignInManager<User> signInManager, RoleManager<UserRole> roleManager)
        {
            this.SignInManager = signInManager;
            this.RoleManager = roleManager;
            this.Storage = storage;
            this.UserManager = userManager;
        }
            public T GetService<T>() where T : ISupremeService
        {
            foreach (FieldInfo t in this.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic))
            {
                if (typeof(T).GetTypeInfo().IsAssignableFrom(t.FieldType) && t.GetType().GetTypeInfo().IsClass)
                {
                    T repo;
                    object value = t.GetValue(this);
                    if (value == null)
                    {
                        repo = (T)Activator.CreateInstance(t.FieldType,this);
                    }
                    else
                        repo = (T)value;
                    return repo;
                }
            }
            return default(T);
        }
    }
}
