﻿using EpamTicketoTask.BusinessLogic.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using EpamTicketoTask.Data.Abstractions;
using EpamTicketoTask.Data.Models;
using Microsoft.AspNetCore.Identity;
using EpamTicketoTask.BusinessLogic.Models.DTO;
using System.Globalization;
using EpamTicketoTask.BusinessLogic.Infrastucture;
using AutoMapper;
using System.Linq;
using System.Threading.Tasks;

namespace EpamTicketoTask.BusinessLogic.Services
{
    class UserService : AbstractService, IUserService
    {
        private SignInManager<User> signInManager;
        private RoleManager<UserRole> roleManager;
        public UserService(ServiceStorage storage) : base(storage)
        {
            this.signInManager = storage.SignInManager;
            this.roleManager = storage.RoleManager;
        }

        public async Task AssignRolesAsync(string userName, IEnumerable<string> roles)
        {
            User user = await userManager.FindByNameAsync(userName);
            if (roles != null)
            {
                foreach (string role in roles)
                {
                    if (!userManager.IsInRoleAsync(user, role).Result)
                    {
                        await userManager.AddToRoleAsync(user, role);
                    }
                }

                IEnumerable<string> rolesToRevoke = roleManager.Roles
                    .Where(r => !roles.Where(x => x == r.Name).Any())
                    .Select(r => r.Name).ToList();
                foreach (string role in rolesToRevoke)
                {
                    if (userManager.IsInRoleAsync(user, role).Result)
                    {
                        await userManager.RemoveFromRoleAsync(user, role);
                    }
                }
            }
            else
            {
                foreach (string role in roleManager.Roles.Select(r => r.Name))
                {
                    if (userManager.IsInRoleAsync(user, role).Result)
                    {
                        await userManager.RemoveFromRoleAsync(user, role);
                    }
                }
            }
        }

        public void Delete(string id)
        {
            throw new NotImplementedException();
        }

        public UserDTO Get(string userName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<UserDTO> GetAll()
        {
            IEnumerable<User> users = userManager.Users.ToList();
            Mapper.Initialize(cfg => cfg.CreateMap<User, UserDTO>()
            .ForMember("Password", o => o.MapFrom(src => src.PasswordHash))
            .ForMember("Roles", o => o.MapFrom(src =>
               userManager.GetRolesAsync(src).Result)));

            return Mapper.Map<IEnumerable<User>, List<UserDTO>>(users);
        }

        public UserDTO GetByName(string userName)
        {
            User curUser = userManager.FindByNameAsync(userName).Result;
            Mapper.Initialize(cfg => cfg.CreateMap<User, UserDTO>()
            .ForMember("Password", o => o.MapFrom(src => src.PasswordHash))
            .ForMember("Roles", o => o.MapFrom(src =>
               userManager.GetRolesAsync(src).Result)));

            return Mapper.Map<User, UserDTO>(curUser);
        }

        public IEnumerable<string> GetRoles(string userName)
        {
            User curUser = userManager.FindByNameAsync(userName).Result;
            return userManager.GetRolesAsync(curUser).Result;
        }

        public void Make(UserDTO userDto)
        {
            if (userManager.FindByNameAsync(userDto.UserName).Result != null)
            {
                throw new ValidationException($"The Username: {userDto.UserName} is alreay Taken", "UserName");
            }
            User user = new User(userDto.UserName, userDto.Password,
              userDto.FirstName, userDto.LastName,
              userDto.Localization);
            userManager.CreateAsync(user).Wait();
            userManager.AddToRoleAsync(user, "user").Wait();
        }

        public void SignIn(string userName, string password, bool remember)
        {
            User curUser = userManager.FindByNameAsync(userName).Result;
            if (curUser == null)
            {
                throw new ValidationException("Incorrect login", "UserName");
            }
            if (!userManager.CheckPasswordAsync(curUser, password).Result)
            {
                throw new ValidationException("Incorrect password", "Password");
            }
            signInManager.SignInAsync(curUser, remember).Wait();
        }

        public void SignOut()
        {
            signInManager.SignOutAsync().Wait();
        }

        public void Update(UserDTO model)
        {
            User user = userManager.FindByNameAsync(model.UserName).Result;
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.Localization = model.Localization;
            user.Address = model.Address;
            user.PhoneNumber = model.PhoneNumber;
            user.Photo = model.Photo;

            userManager.UpdateAsync(user).Wait();
        }
    }
}
