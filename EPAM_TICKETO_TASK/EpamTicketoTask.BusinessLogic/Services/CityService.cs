﻿using System;
using System.Collections.Generic;
using EpamTicketoTask.BusinessLogic.Abstractions;
using EpamTicketoTask.BusinessLogic.Models.DTO;
using EpamTicketoTask.Data.Abstractions;
using EpamTicketoTask.Data.Models;
using Microsoft.AspNetCore.Identity;
using EpamTicketoTask.BusinessLogic.Infrastucture;
using EpamTicketoTask.Data.Abstractions.EntityRepositories;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace EpamTicketoTask.BusinessLogic.Services
{
    public class CityService : AbstractService, ICityService
    {
        private ICityRepository cityRepository;
        public CityService(ServiceStorage storage) : base(storage)
        {
            cityRepository = base.storage.GetRepository<ICityRepository>();
        }

        public void Delete(string id)
        {
            if (id == null)
                throw new ValidationException("Invalid id", "");
            try
            {
                storage.GetRepository<ICityRepository>().Delete(id);
                storage.Save();
            }
            catch (DbUpdateException)
            {
                throw new ValidationException("City has relations", "");
            }
        }

        public CityDTO Get(string id)
        {
            City c = cityRepository.Get(id);
            if (c == null)
                throw new ValidationException("City Not Found", "");
            CityDTO city = new CityDTO
            {
                Id=c.Id,
                Name=c.Name
            };
            return city;
        }

        public IEnumerable<CityDTO> GetAll()
        {
            IEnumerable<CityDTO> cities= cityRepository.All()
                .Select(c => new CityDTO
                {
                    Id = c.Id,
                    Name = c.Name
                });
            return cities;
        }

        public void Make(CityDTO city)
        {
            City resultCity=new City(city.Name);
            cityRepository.Create(resultCity);
            storage.Save();
        }

        public void Update(CityDTO city)
        {
            if (city.Id == null)
                throw new ValidationException("City Not Found", "");
            City resultCity = new City(city.Id,city.Name);
            cityRepository.Update(resultCity);
            storage.Save();
        }
    }
}
